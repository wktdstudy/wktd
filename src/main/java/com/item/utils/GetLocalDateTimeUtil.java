package com.item.utils;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author likaiping
 * @editTime 2024/4/21 14:03
 */
public class GetLocalDateTimeUtil {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * 获取当前时间并格式化
     * @return 格式化为字符串的时间
     */
    public String getMyLocalDateTime() {
        //获取当前时间
        LocalDateTime localDateTime = LocalDateTime.now();
        String dateTime = localDateTime.format(formatter);
        return dateTime;
    }

    /**
     * 将LocalDateTime类型的时间格式化转换为String类型的字符串
     * @param localDateTime 时间
     * @return 字符串类型的时间
     */
    public String getMyLocalDateTimeToString(LocalDateTime localDateTime) {
        String dateTime = localDateTime.format(formatter);
        return dateTime;
    }

    /**
     * 将格式化的字符串时间转换为LocalDateTime类型的时间
     * @param time 字符串的时间
     * @return LocalDateTime类型的时间
     */
    public LocalDateTime shiftStringTimeToLocalDateTime(String time) {
        LocalDateTime localDateTime = LocalDateTime.parse(time, this.formatter);
        return localDateTime;
    }

    /**
     * 传入两个格式化后的字符串时间，将其转换为LocalDateTime类型的时间后计算时间差
     * @param startTimeStr 开始的字符串时间
     * @param endTimeStr 结束的字符串时间
     * @return Duration类型的时间差
     */
    public Duration durationForStringTimes(String startTimeStr, String endTimeStr) {
        LocalDateTime startTime = shiftStringTimeToLocalDateTime(startTimeStr);
        LocalDateTime endTime = shiftStringTimeToLocalDateTime(endTimeStr);
        Duration duration = Duration.between(startTime, endTime);
        return duration;
    }

}
