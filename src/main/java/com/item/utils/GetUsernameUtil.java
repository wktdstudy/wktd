package com.item.utils;

import java.util.Map;

/**
 * @author likaiping
 * @createTime 2024/5/28 22:45
 */
public class GetUsernameUtil {
    public static String getUsername(String token) {
        Map<String, Object> claims = JwtUtil.parseToken(token);
        String username = (String) claims.get("username");
        return username;
    }
}
