package com.item.utils;

import com.item.pojo.ItemComment;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
@Component
public class UserCF {
 
    /**
     * 方法描述: 推荐菜品id列表
     *
     * @param userId 当前用户id
     * @param list 用户美食评分数据
     * @return {@link List<Integer>} 相关系数最接近的用户id列表
     */
    public static List<Integer> recommend(Integer userId, List<ItemComment> list) {
        //按用户分组
        Map<Integer, List<ItemComment>> itemMap=list.stream().collect(Collectors.groupingBy(ItemComment::getUserId));
        //获取其他用户与当前用户的关系值
        Map<Integer,Double>  itemDisMap = CoreMath.computeNeighbor(userId, itemMap,0);
        System.out.println(itemDisMap);
        //获取关系最近物品
        double maxValue= Collections.max(itemDisMap.values());
        System.out.println(maxValue);
        return itemDisMap.entrySet().stream().filter(e->e.getValue()==maxValue).map(Map.Entry::getKey).collect(Collectors.toList());
    }
 
 
}
