package com.item.utils;

import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConsoleBeanFactory {

    @Bean
    public PrintStream consolePrintStream() {
        return new PrintStream(System.out, true, StandardCharsets.UTF_8);
    }
}
