package com.item.utils;

import jakarta.annotation.Resource;
import jakarta.websocket.*;
import jakarta.websocket.server.PathParam;
import jakarta.websocket.server.ServerEndpoint;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author likaiping
 * @createTime 2024/6/3 21:52
 */
@Component
@Slf4j
@ServerEndpoint(value = "/notifyToUser") //暴露的ws应用的路径
public class NotifyToUserWebSocket {

    /*@Resource
    private RedisUtil redisUtil = new RedisUtil();*/
    // 用来存储服务连接对象
    private static Map<String , Session> clientMap = new ConcurrentHashMap<>();

    /**
     * 客户端与服务端连接成功
     * @param session
     */
    @OnOpen
    public void onOpen(Session session/*, @PathParam("username") String username*/){
        /*
            do something for onOpen
            与当前客户端连接成功时
         */
        clientMap.put(session.getId(),session);
        //把该用户的session的id存入redis中
//        redisUtil.set("session_"+username,session.getId());
    }

    /**
     * 客户端与服务端连接关闭
     * @param session
     */
    @OnClose
    public void onClose(Session session/*, @PathParam("username") String username*/){
        /*
            do something for onClose
            与当前客户端连接关闭时
         */
        clientMap.remove(session.getId());
//        redisUtil.delete("session_"+username);
    }

    /**
     * 客户端与服务端连接异常
     * @param error
     * @param session
     */
    @OnError
    public void onError(Throwable error,Session session) {
        error.printStackTrace();
    }

    /**
     * 客户端向服务端发送消息
     * @param message
     * @throws IOException
     */
    @OnMessage
    public void onMsg(Session session,String message) throws IOException {
        /*
            do something for onMessage
            收到来自当前客户端的消息时
         */
        sendAllMessage(message);
    }

    //向所有客户端发送消息（广播）
    public void sendAllMessage(String message){
        Set<String> sessionIdSet = clientMap.keySet(); //获得Map的Key的集合
        System.out.println(sessionIdSet);
        // 此处相当于一个广播操作
        for (String sessionId : sessionIdSet) { //迭代Key集合
            Session session = clientMap.get(sessionId); //根据Key得到value
            session.getAsyncRemote().sendText(message); //发送消息给客户端
            System.out.println("333333333333333333");
            System.out.println(session);
        }
    }
}

