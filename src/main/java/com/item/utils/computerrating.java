package com.item.utils;

import com.item.pojo.ItemComment;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
@Component
public class computerrating {
    public static List<Integer>itemList(List<ItemComment> itemComments){
        Map<Integer, List<ItemComment>> itemMap = itemComments.stream().collect(Collectors.groupingBy(ItemComment::getItemId));
        Map<Integer, Double> distMap = new TreeMap<>();
        itemMap.forEach((k, v) -> {
            double allrating= v.stream().mapToDouble(ItemComment::getRating).sum();
            int count = v.size();
            double rating = allrating / count;
            distMap.put(k, rating);

        });
        System.out.println(distMap);
        return distMap.entrySet().stream().sorted((o1, o2) -> o2.getValue().compareTo(o1.getValue())).map(Map.Entry::getKey).collect(Collectors.toList());
    }
}
