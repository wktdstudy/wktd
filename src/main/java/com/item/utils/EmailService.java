package com.item.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class EmailService {


    @Value("${spring.mail.username}")
    private String senderEmail;
    @Autowired
    @Qualifier("javaMailSender1")
   private JavaMailSender javaMailSender;

    public void sendEmail(String recipientEmail, String subject, String content) {
        try {
            if (javaMailSender == null) {
                System.out.println("JavaMailSender is null,can not send");
                return;
            }

            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(senderEmail); // 设置发送者邮箱地址
            helper.setTo(recipientEmail);
            helper.setSubject(subject);
            helper.setText(content);
            javaMailSender.send(message);
            System.out.println("邮件发送成功！");
        } catch (javax.mail.MessagingException e) {
            e.printStackTrace();
            System.out.println("邮件发送失败：" + e.getMessage());
        }
    }

}
