package com.item.utils;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class redislistenerutil {
    private RedisTemplate<String, Object> redisTemplate;
    private RedisUtil redisUtil;
    @Bean
    public MessageListener userMessageListener() {
        return (message, pattern) -> {
            String receivedMessage = (String) redisTemplate.getValueSerializer().deserialize(message.getBody());
            System.out.println("User received message: " + receivedMessage);
            String userstringid= (String) redisTemplate.opsForValue().get("userid2");
            if(receivedMessage!=null){
                redisUtil.hashSet("usermessage",userstringid,receivedMessage,60*60*24*7);
            }


        };
    }
    @Bean
    public MessageListener ortheruserMessageListener() {
        return (message, pattern) -> {
            String receivedMessage = (String) redisTemplate.getValueSerializer().deserialize(message.getBody());
            System.out.println("ortherUser received message: " + receivedMessage);
            if(receivedMessage!=null){
                redisUtil.hashSet("itemmessage", String.valueOf(1),receivedMessage,60*60*24*7);
            }


        };
    }
    @Bean
    public MessageListener couponUserMessageListener() {
        return (message, pattern) -> {
            String receivedMessage = (String) redisTemplate.getValueSerializer().deserialize(message.getBody());
            System.out.println("couponUser received message: " + receivedMessage);
            String userstringid= (String) redisTemplate.opsForValue().get("userId3");
            if(receivedMessage!=null){
                redisUtil.hashSet("couponmessage",userstringid,receivedMessage,60*60*24*7);
            }


        };
    }
}
