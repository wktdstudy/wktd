package com.item.utils;

import com.item.pojo.ItemComment;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
@Component
public class ItemCF {

    /**
     * 方法描述: 推荐菜品id列表
     *
     * @param itemId 当前菜品id
     * @param list   用户菜品评分数据
     * @return {@link List<Integer>}
     */
    public static List<Integer> recommend(Integer itemId, List<ItemComment> list) {
        //按物品分组
        Map<Integer, List<ItemComment>> itemMap = list.stream().collect(Collectors.groupingBy(ItemComment::getItemId));
        //获取其他物品与当前物品的关系值
        Map<Integer, Double> itemDisMap = CoreMath.computeNeighbor(itemId, itemMap, 1);
        System.out.println(itemDisMap);
        //获取关系最近物品
        double maxValue = Collections.max(itemDisMap.values());
        System.out.println(maxValue);
        return itemDisMap.entrySet().stream().filter(e -> e.getValue() == maxValue).map(Map.Entry::getKey).collect(Collectors.toList());
    }
}
 
