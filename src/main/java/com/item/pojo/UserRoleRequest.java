package com.item.pojo;

import lombok.Data;

import java.util.List;

// 创建一个DTO类来映射请求体
@Data
public
class UserRoleRequest {
    private List<Integer> roleid;
    private int userid;

    // Getter & Setter
}