package com.item.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 设置响应类型，状态码，响应数据
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Result<T> {
   //状态码
   private Integer code;
   //操作信息
   private String message;
   //响应数据
   private T date;

   public static <T> Result<T> success(T date){return new Result<>(200,"完成操作",date);}

   public static Result success(){return new Result(200,"操作成功",null);}

   public  static Result error(String message){return new Result(1,message,null);}

   /**
    * 返回message和date
    */
   public static <T> Result<T> success(String message,T date){
      return new Result<>(200,message,date);
   }
}
