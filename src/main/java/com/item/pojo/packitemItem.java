package com.item.pojo;

import lombok.Data;

@Data
public class packitemItem {
    private int id;
    private int itemId;
    private int packId;
}
