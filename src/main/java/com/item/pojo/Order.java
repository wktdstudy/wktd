package com.item.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class Order {
    private int id;
    private int userId;
    private LocalDateTime ordertime;
    private BigDecimal totalamount;
    private String status;
}
