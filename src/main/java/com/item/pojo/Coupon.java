package com.item.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;


@Data
public class Coupon {


    private int id;


    private String description;


    private BigDecimal discountValue;

    private Integer minimumSpend;


    private BigDecimal maximumSpend;

    private LocalDateTime startDate;

    private LocalDateTime endDate;
    private int number;
    private int version;
    private String member;
    private int isdelete;
}
