package com.item.pojo;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * Represents a user coupon record in the user_coupons table.
 */
@Data
public class UserCoupon {

    /**
     * The unique identifier for the user coupon.
     */
    private int id;

    /**
     * The identifier for the user.
     */
    private int userId;

    /**
     * The identifier for the coupon.
     */
    private int couponId;

    /**
     * The date and time when the coupon was redeemed.
     */
    private LocalDateTime redeemDate;

    /**
     * The status of the coupon.
     */
    private String status;
}
