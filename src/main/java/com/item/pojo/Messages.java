package com.item.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * 
 * @TableName messages
 */
@TableName(value ="messages")
@Data
public class Messages implements Serializable {
    /**
     * 
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    @TableField(value = "from_user_name")
    private String fromUserName;

    /**
     * 
     */
    @TableField(value = "messages_text")
    private String messagesText;

    /**
     * 
     */
    @TableField(value = "messages_type")
    private String messagesType;

    /**
     * 
     */
    @TableField(value = "add_time")
    private LocalDateTime addTime;

    /**
     * 
     */
    @TableField(value = "to_user_name")
    private String toUserName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}