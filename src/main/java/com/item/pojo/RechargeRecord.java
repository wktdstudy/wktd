package com.item.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * 
 * @author XXDLD
 * @TableName recharge_record
 */
@TableName(value ="recharge_record")
@Data
public class RechargeRecord implements Serializable {
    /**
     * 
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Double id;

    /**
     * 
     */
    @TableField(value = "user_id")
    private Integer userId;

    /**
     * 
     */
    @TableField(value = "recharge_time")
    private LocalDateTime rechargeTime;

    /**
     * 
     */
    @TableField(value = "balance_record")
    private BigDecimal balanceRecord;

    /**
     * 
     */
    @TableField(value = "balance")
    private BigDecimal balance;

    /**
     * 
     */
    @TableField(value = "status")
    private String status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}