package com.item.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * 
 * @TableName computerusage
 */
@TableName(value ="computerusage")
@Data
public class Computerusage implements Serializable {
    /**
     * 
     */
    @TableId(value = "id")
    private Integer id;

    /**
     * 
     */
    @TableField(value = "user_id")
    private Integer userId;

    /**
     * 
     */
    @TableField(value = "starttime")
    private LocalDateTime starttime;

    /**
     * 
     */
    @TableField(value = "endtime")
    private LocalDateTime endtime;

    /**
     * 
     */
    @TableField(value = "duration")
    private Double duration;

    /**
     * 
     */
    @TableField(value = "cost")
    private BigDecimal cost;

    /**
     * 
     */
    @TableField(value = "computer_id")
    private Integer computerId;
    /**
     * 逻辑删除 0为未删除 1为已删除状态
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}