package com.item.pojo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class itempack {
    private int id;
    private String description;
    private BigDecimal allprice;
    private String imageUrl;
    private int isdelete;

}
