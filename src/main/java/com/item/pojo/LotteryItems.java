package com.item.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName lottery_items
 */
@TableName(value ="lottery_items")
@Data
public class LotteryItems implements Serializable {
    /**
     * 
     */
    @TableId(value = "lottery_Items_id", type = IdType.AUTO)
    private Integer lotteryItemsId;

    /**
     * 
     */
    @TableField(value = "title")
    private String title;

    /**
     *
     */
    @TableField(value = "probability")
    private Double probability;

    /**
     * 
     */
    @TableField(value = "description")
    private String description;

    /**
     * 
     */
    @TableField(value = "value")
    private String value;

    /**
     *
     */
    @TableField(value = "number")
    private Integer number;

    /**
     *
     */
    @TableField(value = "type")
    private Integer type;

    /**
     *
     */
    @TableField(value = "status")
    private Integer status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}