package com.item.pojo;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class MenuItem {

    private int id;
    private String itemname;
    private BigDecimal price;
    private BigDecimal specialPrice;
    private String description;
    private String imageUrl;
    private String type;
    private int version;
    private int quantity;
    private int sales;
    private int isdelete;
}