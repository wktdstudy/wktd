package com.item.pojo;

import java.util.Date;
import lombok.Data;

@Data
public class ItemComment {
    private int id;
    private String comment;
    private Date commentTime;
    private double rating;
    private Integer pid;
    private Integer userId;
    private Integer itemId;
    private int isdelete;
    private  int weight;
    private int isreply;
}
