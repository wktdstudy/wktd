package com.item.pojo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SpecialMenu {
    private int id;
    private String itemName;
    private BigDecimal price;
    private BigDecimal specialPrice;
    private String quantity;
    private String imageUrl;
    private Integer version;
}
