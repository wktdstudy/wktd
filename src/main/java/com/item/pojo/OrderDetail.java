package com.item.pojo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderDetail {
    private int id;
    private int orderId;
    private int itemId;
    private int quanntite;
    private BigDecimal unitprice;
}
