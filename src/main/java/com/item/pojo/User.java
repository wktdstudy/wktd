package com.item.pojo;



import jakarta.validation.constraints.Email;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
@ToString
@Getter
@Setter
public class User {
    private int id;
    private String username;
    private String password;
    @Email
    private String email;
    private Integer age;
    private String phone;
    private String idCard;
    private String verificationCode;
    private String address;
    private  String sex;
    //创建时间
    private LocalDateTime createTime;
    //更新时间
    private LocalDateTime updateTime;
    private BigDecimal balance;
    BigDecimal amount;
    private String imageurl;
    int banned;
    private Integer points;
    private LocalDate lastCheckinDate;
}
