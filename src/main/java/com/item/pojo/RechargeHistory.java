package com.item.pojo;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author XXDLD
 */
@Data
public class RechargeHistory implements Serializable {

    private String username;

    private Double id;

    private Integer userId;

    private LocalDateTime rechargeTime;

    private BigDecimal balanceRecord;

    private BigDecimal balance;

    private String status;

}