package com.item.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * 
 * @TableName coupons
 */
@TableName(value ="coupons")
@Data
public class Coupons implements Serializable {
    /**
     * 
     */
    @TableId(value = "id")
    private Integer id;

    /**
     * 
     */
    @TableField(value = "description")
    private String description;

    /**
     * 
     */
    @TableField(value = "discount_value")
    private BigDecimal discountValue;

    /**
     * 
     */
    @TableField(value = "minimum_spend")
    private Integer minimumSpend;

    /**
     * 
     */
    @TableField(value = "maximum_spend")
    private Integer maximumSpend;

    /**
     * 
     */
    @TableField(value = "start_date")
    private LocalDateTime startDate;

    /**
     * 
     */
    @TableField(value = "end_date")
    private LocalDateTime endDate;

    /**
     * 
     */
    @TableField(value = "number")
    private Integer number;

    /**
     * 
     */
    @TableField(value = "version")
    private Integer version;

    /**
     * 
     */
    @TableField(value = "member")
    private String member;

    /**
     * 
     */
    @TableField(value = "title")
    private String title;

    /**
     * 
     */
    @TableField(value = "showDialog")
    private String showdialog;

    /**
     *
     */
    @TableField(value = "price")
    private Integer price;

    /**
     *
     */
    @TableField(value = "status")
    private Integer status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}