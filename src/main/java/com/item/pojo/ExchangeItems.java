package com.item.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName exchange_items
 */
@TableName(value ="exchange_items")
@Data
public class ExchangeItems implements Serializable {
    /**
     * 
     */
    @TableId(value = "exchange_items_id", type = IdType.AUTO)
    private Integer exchangeItemsId;

    /**
     * 
     */
    @TableField(value = "title")
    private String title;

    /**
     * 
     */
    @TableField(value = "description")
    private String description;

    /**
     * 
     */
    @TableField(value = "price")
    private Integer price;

    /**
     * 
     */
    @TableField(value = "showDialog")
    private String showdialog;

    /**
     * 
     */
    @TableField(value = "type")
    private String type;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}