package com.item.pojo;

import java.math.BigDecimal;

public class OrderDetailsResponse {
    private String itemname;
    private BigDecimal price;
    private int quantity;
private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String imageUrl;
    public void setCount(boolean count) {
        isCount = count;
    }

    public boolean isCount() {
        return isCount;
    }

    private boolean isCount=false;
    // 构造函数
    public OrderDetailsResponse(String itemname, BigDecimal price, int quantity) {
        this.itemname = itemname;
        this.price = price;
        this.quantity = quantity;
    }

    public OrderDetailsResponse() {

    }

    // getter 和 setter 方法
    public String getItemName() {
        return itemname;
    }

    public void setItemName(String itemname) {
        this.itemname = itemname;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
