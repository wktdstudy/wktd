package com.item.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


import java.util.Date;

@Getter
@Setter
@ToString
public class Role {
    private Long id;
    private String roleName;
    private Boolean isDeleted;
    private String createdBy;
    private String updatedBy;
    private Date createTime;
    private Date updateTime;
}
