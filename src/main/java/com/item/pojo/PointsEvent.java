package com.item.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * 
 * @TableName points_event
 */
@TableName(value ="points_event")
@Data
public class PointsEvent implements Serializable {
    /**
     * 
     */
    @TableId(value = "id")
    private Integer id;

    /**
     * 
     */
    @TableField(value = "username")
    private String username;

    /**
     * 
     */
    @TableField(value = "event")
    private String event;

    /**
     * 
     */
    @TableField(value = "event_time")
    private LocalDateTime eventTime;

    /**
     * 
     */
    @TableField(value = "points")
    private String points;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}