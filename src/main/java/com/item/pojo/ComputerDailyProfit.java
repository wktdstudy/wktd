package com.item.pojo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 电脑使用的每日收益 实体类
 * @author likaiping
 * @createTime 2024/6/10 14:38
 */
public class ComputerDailyProfit {
    private Date date;
    private BigDecimal totalProfit;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(BigDecimal totalProfit) {
        this.totalProfit = totalProfit;
    }
}
