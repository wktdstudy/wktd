package com.item.pojo;

import lombok.Data;

import java.time.LocalDate;

/**
 * @author XXDLD
 */
@Data
public class LotteryRecords {
    private String username;
    private String lotteryItemsTitle;
    private LocalDate date;
}
