package com.item.service;

import com.item.pojo.Answer;
import com.item.pojo.Question;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author XXDLD
* @description 针对表【question】的数据库操作Service
* @createDate 2024-05-17 14:58:35
*/
public interface QuestionService extends IService<Question> {
     List<Question> getQuestionByHits();

     List<Answer> getAnswerByQuestionId(Integer questionId);

     List<Question> getQuestionByContent(String content);
}
