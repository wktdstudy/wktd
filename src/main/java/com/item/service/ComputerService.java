package com.item.service;

import com.item.pojo.Computer;
import com.baomidou.mybatisplus.extension.service.IService;
import com.item.pojo.Computerusage;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
* @author likaiping
* @description 针对表【computer】的数据库操作Service
* @createDate 2024-04-21 12:11:27
*/
public interface ComputerService extends IService<Computer> {
    /**
     * 模糊查询电脑列表 筛选
     * @param price 价格
     * @param detail 电脑类型
     * @return computer的list集合
     */
     List<Computer> findListSearchComputer(BigDecimal price, String detail);

    /**
     * 插入初始数据都爱computerusage表 表示用户开始上机
     * @param username 用户名
     * @param detail 电脑类型
     * @return /
     */
    Map<String, String> insertComputerUsage(String username, String detail);

    /**
     * 下机操作 点击下机后传入用户名和电脑机型
     * @param username 用户名
     * @param detail 电脑机型
     * @return 花费的金额
     */
     BigDecimal useEndAndEditComputerUsage(String username, String detail);

    /**
     * 根据用户名获取该用户所有历史上机记录
     * @param username 用户名
     * @return 历史记录
     */
     List<Computerusage> findComputerUsageListByUserId(String username);

     /**
     * 根据时间范围查询用户所有历史上机记录
     * @param firstTime 开始时间
     * @param secondTime 结束时间
     * @param username 用户名
     * @return 历史记录
     */
     List<Computerusage> findHistoryByTimeToTime(String firstTime, String secondTime, String username);

    /**
     * 根据用户名和电脑信息查询可用时长和余额
     * @param username 用户名
     * @param detail 电脑信息
     * @return 存储可用时长和余额的map
     */
     Map<String ,String> findUsableTimeAndBalance(String username, String detail);

    /**
     * 添加电脑
     * @param computer 电脑信息
     * @return 布尔值
     */
     Boolean addComputer(Computer computer);

    /**
     * 根据电脑机型删除电脑
     * @param id 电脑id
     * @return 布尔值
     */
     boolean removeComputer(Integer id);

    /**
     * 编辑电脑
     * @param computer 电脑信息
     * @return 布尔值
     */
     boolean editComputer(Computer computer);
}
