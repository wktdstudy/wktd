package com.item.service;

import com.item.pojo.RechargeHistory;
import com.item.pojo.RechargeRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import com.item.pojo.Result;

import java.util.List;
import java.util.Map;

/**
* @author XXDLD
* @description 针对表【recharge_record】的数据库操作Service
* @createDate 2024-04-24 20:39:46
*/
public interface RechargeRecordService extends IService<RechargeRecord> {
    boolean insertRechargeRecord(Long tradeId,double record, String username);

    List<RechargeRecord> getRechargeRecordByUsersId(String username);

    List<RechargeHistory> getRechargeHistory();
}
