package com.item.service;

import com.item.pojo.*;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public interface foodservice {
     List<MenuItem> getfood(String type) ;
     void insertto0rder(BigDecimal price, int id, int userid,int ammount);
    List<OrderDetailsResponse>  getorder(int userid);
    BigDecimal getorderamount(int userid);
    BigDecimal changeamountbycoupon(int userid,int couponid);
    void deletOrderDetailByitemId(int itemId,int userid);
    Boolean ConfirmOrder(int userid);
    boolean Modifysales(int itemId,int userid);
    List<MenuItem> searchfood(String itemname);
    List<MenuItem> findspecialmenu(String type);
    Boolean Spikesmenu(int id,int userid,BigDecimal price,int ammount);
    Boolean SpikesmenuRedis(int itemid,int userid);
    Boolean ModifySpecialsales(int itemId,int userid);
    List<itempack> finditempack();
    List<ItemComment> getcomment(int itemId);
    List<ItemComment> getdowncomment(int id,int itemId);
    User getuserdetail(int userId);
    void insertcomment(String comment, double rating, int pid, int userId, int itemId);
    void finshapipalyorder( int userId);
    List<OrderDetailsResponse> findordetailbyuserIdandnumber(int userId);
    BigDecimal findtotalamountByuseridandnumber(int userId);
    int findorderidByuseridandnumber(int userId);
    String findorderstatusByuseridandnumber(int userId);
    List<Order> findorderByuserid( int userId);
    List<MenuItem> finditemlistByOrderId(int orderId);
    List<OrderDetailsResponse> findordetailbyorderId(int userId);
    BigDecimal findtotalamountByorderId(int userId);
    String findorderstatusByorderId(int userId);
    List<Coupon> getcouponByuserId(int userId);
    List<Coupon>getallcoupons();
    List<Role> getallrole();
    Boolean getcoupons(int couponId);
    List<Integer>getcommenditmeId(int userId);
    List<MenuItem> getmenurecommendbyuserId(List<Integer> userId);
    Boolean insertorderdetailBypackItem(int userId,BigDecimal totalamount,int packId);
    List<MenuItem> getListmenuBypackId(int packId);
    List<Integer> getitemIdByrecomend(int itemId);
    List<MenuItem> menurecommendByitemId(List<Integer> itemId);
    List<MenuItem> menuListBysales();
    List<Integer> menuidListByAveragerating();
    List<MenuItem> menuListByavagerItemList(List<Integer> itemId);
    Boolean deletorderanorderdetail(int orderId);

// 从这里开始时food的后台service
    List<User> getAlluserlist();

    Boolean updatepassword(String username,String password);
    Boolean updateuser(int id,String username, String email,  String phone, String imageurl,int age);
    List<Role> getallRolemange();
    List<Role> getallroleByusername(String username);
  Boolean  insertuserRoleByuseridandroleid(int userid,List<Integer> roleid);
  Boolean updateUserstatus(int userid);
  List<MenuItem> getallmenuList();
  Boolean insertmenuBybakground(MenuItem menuItem);
  MenuItem getmenuByid(int id);
  Boolean updateitemByitem(MenuItem menuItem);
  Boolean updateitemspecialPriceandtypeandquantityByitemId(MenuItem menuItem);
  Boolean updateitemstatusByitemId(int itemId);
  Boolean updateitempriceByitemid( int itemId,BigDecimal price);
    List<Order> getorderBybackground();
    Boolean updateorderstatusByorderId(int orderId,String status);
    Boolean diccountitemorice(BigDecimal Discount);
    public Boolean diccountitemoriceBytype(String type, BigDecimal Discount);
    Boolean recoveritemorice();
    List<Coupon> getallcouponsBybackground();
    Boolean insertcoupon( Coupon coupon);
    Coupon getcouponByid(int id);

    Boolean updatecoupon(Coupon coupon);
    Boolean setcouponTouser(int couponId,int userId);
    List<User> finduserBYcouponId( int couponId);
    Boolean deleteUsercouponByuserIdandcouponId(int couponId,int userId);
    Boolean deletcouponByid(int id);
    List<ItemComment> getallcomment();
    Boolean insertcommentByidanditemid( String comment,int pid,int itemId);
    Boolean updatecommentweightByid(int id,int itemId);
    Boolean updatecommentisdeleteByid( int id);
    List<itempack> getallitempackBybackground();
    Boolean insertitempackBybakground(String description,String imageUrl);
   Boolean updateitempackdescriptionByid( int id,String description,String imageUrl);
   BigDecimal getallpriceBypackId(int packId);
   Boolean deletepackitemItemByitemIdandpackId( int itemId,int packId);
   Boolean insertpackitemItemByitemIdandpackId( int itemId,int packId);
   Boolean updateitempackallpriceByid( int id,BigDecimal price);
   Boolean updeteitempackallpriceByaccountandid( int id,BigDecimal account);
Boolean updateallitempackallpriceByaccountandid(BigDecimal account);
Boolean updateitempackisdeleteByid( int id);
    BigDecimal gettaldayorderaomunt();
    BigDecimal getyesterdayorderaomunt();
    BigDecimal getbetterOrderpercent();
    List<MenuItem> menulistBysalsasLimteten();
List<ItemComment> itemcommentListByuserId(int userId);
List<ItemComment> getitemcommentByuserIdandpid(int pid);
Coupon getbestcoupon(BigDecimal price,int userId);
Boolean deleteorderandorderDetailByuserId(int userId);
}
