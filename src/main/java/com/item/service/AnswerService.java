package com.item.service;

import com.item.pojo.Answer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author XXDLD
* @description 针对表【answer】的数据库操作Service
* @createDate 2024-05-17 15:23:50
*/
public interface AnswerService extends IService<Answer> {

}
