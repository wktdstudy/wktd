package com.item.service;

import com.item.pojo.Auth;
import com.item.pojo.Role;
import com.item.pojo.User;
import org.springframework.stereotype.Service;


import java.util.List;
@Service
public interface roleservice {

    List<Role> findrolebyusername(String username);
    User getuser(String username);
    List<Auth> findauthbyusername(String username);

    /**
     * 查询所有角色
     * @return 角色列表
     */
    List<Role> findAllRoleList();

    /**
     * 添加角色
     * @param roleName 角色名
     * @param createUsername 创建者用户名
     * @return 布尔值
     */
    boolean addRole(String roleName,String createUsername);

    /**
     * 删除角色
     * @param roleName 需要删除的角色名称
     * @return 布尔值
     */
    boolean removeRoleByRoleName(String roleName);

    /**
     * 修改角色名
     * @param roleName 需要修改的原角色名称
     * @param newName 新名称
     * @param updateUsername 修改人的用户名
     * @return 布尔值
     */
    boolean editRoleName(String roleName, String newName , String updateUsername);

    /**
     * 获取角色父级权限列表
     * @param roleName 角色名
     * @return 角色的父级权限列表
     */
    List<Auth> getRoleParentAuthList(String roleName);

    /**
     * 获取角色权限列表
     * @param roleName 角色名
     * @return 角色的权限列表
     */
    List<Auth> getRoleAuthList(String roleName);

    /**
     * 为角色添加权限组
     * @param roleName 角色名称
     * @param authPidName 权限组名称
     * @return 布尔值
     */
    boolean addAuthsToRole(String roleName, String authPidName);

    /**
     * 删除角色权限组
     * @param roleName 角色名称
     * @param authPidName 权限组名称
     * @return 布尔值
     */
    boolean removeAuthsGroupOfRole(String roleName, String authPidName);

    /**
     * 删除角色所有权限
     * @param roleName 角色名称
     * @return 布尔值
     */
    boolean removeAllAuthsOfRole(String roleName);

    /**
     * 为角色添加所有权限
     * @param roleName 角色名称
     * @return 布尔值
     */
    boolean addAllAuthsToRole(String roleName);

    /**
     * 禁用角色
     * @param roleName 角色名称
     * @return 布尔值
     */
    boolean forbiddenRole(String roleName);

    /**
     * 解禁角色
     * @param roleName 角色名称
     * @return 布尔值
     */
    boolean unForbiddenRole(String roleName);
}
