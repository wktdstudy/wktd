package com.item.service;

import com.item.pojo.Coupons;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author XXDLD
* @description 针对表【coupons】的数据库操作Service
* @createDate 2024-06-05 12:22:44
*/
public interface CouponsService extends IService<Coupons> {

}
