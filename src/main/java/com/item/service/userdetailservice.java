package com.item.service;

import org.springframework.security.core.userdetails.UserDetails;

public interface userdetailservice {
    public UserDetails loaduserdetail(String username);
}
