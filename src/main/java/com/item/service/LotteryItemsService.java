package com.item.service;

import com.item.pojo.LotteryItems;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author XXDLD
* @description 针对表【lottery_items】的数据库操作Service
* @createDate 2024-06-05 15:30:19
*/
public interface LotteryItemsService extends IService<LotteryItems> {

}
