package com.item.service;

import com.item.pojo.Auth;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author likaiping
* @description 针对表【auth(系统菜单)】的数据库操作Service
* @createDate 2024-06-04 17:32:03
*/
public interface AuthService extends IService<Auth> {

    /**
     * 获取父级权限列表
     * @return 父级权限列表
     */
    List<Auth> getParentAuthList();

    /**
     * 根据父级权限名获取子级权限列表
     * @param pidName 父级权限名
     * @return 子级权限列表
     */
    List<Auth> getChildAuthsListByPidName(String pidName);

    /**
     * 更新权限信息
     * @param authId 权限id
     * @param name 权限名
     * @param uri 权限路径
     * @return 是否更新成功
     */
    boolean updateAuthInfo(Long authId, String name, String uri);

    /**
     * 禁用权限
     * @param authId 权限id
     * @return 禁用结果
     */
    boolean forbiddenAuth(Long authId);

    /**
     * 启用权限
     * @param authId 权限id
     * @return 启用结果
     */
    boolean unForbiddenAuth(Long authId);

    /**
     * 添加父级权限
     * @param name 权限名
     * @param uri 权限uri
     * @return 插入结果
     */
    boolean addParentAuth(String name, String uri);

    /**
     * 添加子级权限
     * @param pidName 父级权限名
     * @param name 子权限名称
     * @param uri 子权限uri
     * @return 插入结果
     */
    boolean addChildAuthInParentAuth(String pidName, String name, String uri);

    /**
     * 删除父级权限及其子权限
     * @param pidName 父级权限名称
     * @return 删除结果
     */
    boolean removeParentAuth(String pidName);

    /**
     * 删除子级权限
     * @param authName 子级权限名称
     * @return 删除结果
     */
    boolean removeChildAuth(String authName);
}
