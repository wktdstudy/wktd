package com.item.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.item.pojo.Image;
import com.item.service.ImageService;
import com.item.mapper.ImageMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author likaiping
* @description 针对表【image】的数据库操作Service实现
* @createDate 2024-06-09 15:18:50
*/
@Service
@AllArgsConstructor
public class ImageServiceImpl extends ServiceImpl<ImageMapper, Image>
    implements ImageService{
    private ImageMapper imageMapper;
    @Override
    public List<Image> findImageListByType(String type) {
        return imageMapper.selectImageListByType(type);
    }

    @Override
    public List<Image> findAllImage() {
        return imageMapper.selectAllImage();
    }

    @Override
    public boolean forbiddenImage(String imgName) {
        Long id = imageMapper.selectIdByName(imgName);
        if (id != null){
            return imageMapper.forbiddenImage(id);
        }
        return false;
    }

    @Override
    public boolean unForbiddenImage(String imgName) {
        Long id = imageMapper.selectIdByName(imgName);
        if (id != null){
            return imageMapper.unForbiddenImage(id);
        }
        return false;
    }

    @Override
    public boolean addImage(String name, String imgUrl) {
        return imageMapper.insertImage(name, imgUrl);
    }

    @Override
    public boolean editImage(Image image) {
        return imageMapper.updateImage(image);
    }
}




