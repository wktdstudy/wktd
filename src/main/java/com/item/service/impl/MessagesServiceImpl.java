package com.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.item.pojo.Messages;
import com.item.service.MessagesService;
import com.item.mapper.MessagesMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author XXDLD
* @description 针对表【messages】的数据库操作Service实现
* @createDate 2024-05-30 15:34:45
*/
@Service
public class MessagesServiceImpl extends ServiceImpl<MessagesMapper, Messages>
    implements MessagesService{

    @Override
    public Boolean putMessagesByuserName(String fromUserName, String text, String type, String toUserName) {
        return baseMapper.putMessagesByuserName(fromUserName,text,type,toUserName);
    }

    @Override
    public List<Messages> getMessagesByUserNames(String fromUserName, String toUserName) {
        return baseMapper.getMessagesByUserNames(fromUserName,toUserName);
    }
}




