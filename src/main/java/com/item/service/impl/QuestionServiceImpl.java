package com.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.item.pojo.Answer;
import com.item.pojo.Question;
import com.item.service.QuestionService;
import com.item.mapper.QuestionMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author XXDLD
* @description 针对表【question】的数据库操作Service实现
* @createDate 2024-05-17 14:58:35
*/
@Service
public class QuestionServiceImpl extends ServiceImpl<QuestionMapper, Question>
    implements QuestionService{

    @Override
    public List<Question> getQuestionByHits() {
        return baseMapper.getQuestionByHits();
    }

    @Override
    public List<Answer> getAnswerByQuestionId(Integer questionId) {
        baseMapper.updateQuestionHitsById(questionId);
        return baseMapper.getAnswerByQuestionId(questionId);
    }

    @Override
    public List<Question> getQuestionByContent(String content) {
        return baseMapper.getQuestionByContent(content);
    }
}




