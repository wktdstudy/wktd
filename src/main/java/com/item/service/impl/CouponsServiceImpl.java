package com.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.item.pojo.Coupons;
import com.item.service.CouponsService;
import com.item.mapper.CouponsMapper;
import org.springframework.stereotype.Service;

/**
* @author XXDLD
* @description 针对表【coupons】的数据库操作Service实现
* @createDate 2024-06-05 12:22:44
*/
@Service
public class CouponsServiceImpl extends ServiceImpl<CouponsMapper, Coupons>
    implements CouponsService{

}




