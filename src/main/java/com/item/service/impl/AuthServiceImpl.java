package com.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.item.pojo.Auth;
import com.item.service.AuthService;
import com.item.mapper.AuthMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author likaiping
* @description 针对表【auth(系统菜单)】的数据库操作Service实现
* @createDate 2024-06-04 17:32:03
*/
@Service
@AllArgsConstructor
public class AuthServiceImpl extends ServiceImpl<AuthMapper, Auth>
    implements AuthService{
    private AuthMapper authMapper;
    @Override
    public List<Auth> getParentAuthList() {
        return authMapper.selectParentAuthList();
    }

    @Override
    public List<Auth> getChildAuthsListByPidName(String pidName) {
        Long pid = authMapper.selectIdByPidName(pidName);
        if (pid != null) {
            return authMapper.selectChildAuthsListByPid(pid);
        }else {
            throw new RuntimeException("该父级权限不存在");
        }
    }

    @Override
    public boolean updateAuthInfo(Long authId, String name, String uri) {
        if (authMapper.updateAuthInfo(authId,name,uri)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean forbiddenAuth(Long authId) {
        Long pid = authMapper.selectPidByAuthId(authId);
        if (pid == 0) {
            //说明是父级权限，则需要同时禁用它的子级权限
            boolean isForbidden = authMapper.forbiddenAuth(authId);
            List<Long> childAuthsList = authMapper.listAuthIdsByPid(authId);
            if (authMapper.forbiddenChildAuths(authId,childAuthsList) && isForbidden) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean unForbiddenAuth(Long authId) {
        Long pid = authMapper.selectPidByAuthId(authId);
        if (pid == 0) {
            //说明是父级权限，则需要同时解禁它的子级权限
            boolean isForbidden = authMapper.unForbiddenAuth(authId);
            List<Long> childAuthsList = authMapper.listAuthIdsByPid(authId);
            if (authMapper.unForbiddenChildAuths(authId,childAuthsList) && isForbidden) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean addParentAuth(String name, String uri) {
        if (authMapper.insertParentAuth(name,uri)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean addChildAuthInParentAuth(String pidName, String name, String uri) {
        Long pid = authMapper.selectIdByPidName(pidName);
        Long isPid = authMapper.selectPidByAuthId(pid);
        if (isPid == 0) {
            //说明是父级权限 确认pidName是父级权限，则给该父级权限增加子权限
            if (authMapper.insertChildAuthInParentAuth(pid,name,uri)) {
                //如果插入子权限成功 则更新父级权限的count_sub字段自加一
                //并且查询该新增的子权限的id和查询：如果role_auth表中有这个父级权限的id，则查出对应的roleId，并给这些roleId增加该子权限
                    //父级权限的count_sub字段自加一
                    if (authMapper.updateParentAuthCountSub(pid)) {
                        //查询新增子权限的id
                        Long authId = authMapper.selectIdByPidName(name);
                        //查询拥有该父权限的角色id
                        List<Long> roleIdList = authMapper.selectRoleIdByPidAuthId(pid);
                        if (!roleIdList.isEmpty()) {
                            authMapper.insertAuthToRoles(roleIdList,authId);
                            return true;
                        }
                        return true;
                    }
                }

            }
        return false;
    }

    @Override
    public boolean removeParentAuth(String pidName) {
        Long pid = authMapper.selectIdByPidName(pidName);
        if(authMapper.selectPidByAuthId(pid) == 0) {
            //pid=0 说明是父级权限 先查询是否有子权限，有则先删除它的所有子权限 再删除该权限
            List<Auth> list = authMapper.selectChildAuthsListByPid(pid);
            if (list.isEmpty()) {
                if (authMapper.deleteParentAuth(pid)) {
                    return true;
                }
            }
            if (authMapper.deleteChildAuthsInParentAuth(pid)) {
                if (authMapper.deleteParentAuth(pid)) {
                    return true;
                }
            }

        }
        return false;
    }

    @Override
    public boolean removeChildAuth(String authName) {
        Long authId = authMapper.selectIdByPidName(authName);
        if (authMapper.selectPidByAuthId(authId) != 0) {
            //pid!=0 说明不是父级权限 可以删除该子权限
            if (authMapper.deleteAnChildAuth(authId)) {
                return true;
            }
        }
        return false;
    }
}




