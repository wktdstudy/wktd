package com.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.item.pojo.LotteryItems;
import com.item.service.LotteryItemsService;
import com.item.mapper.LotteryItemsMapper;
import org.springframework.stereotype.Service;

/**
* @author XXDLD
* @description 针对表【lottery_items】的数据库操作Service实现
* @createDate 2024-06-05 15:30:19
*/
@Service
public class LotteryItemsServiceImpl extends ServiceImpl<LotteryItemsMapper, LotteryItems>
    implements LotteryItemsService{

}




