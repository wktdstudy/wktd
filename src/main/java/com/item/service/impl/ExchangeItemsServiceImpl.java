package com.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.item.pojo.ExchangeItems;
import com.item.service.ExchangeItemsService;
import com.item.mapper.ExchangeItemsMapper;
import org.springframework.stereotype.Service;

/**
* @author XXDLD
* @description 针对表【exchange_items】的数据库操作Service实现
* @createDate 2024-06-05 12:23:09
*/
@Service
public class ExchangeItemsServiceImpl extends ServiceImpl<ExchangeItemsMapper, ExchangeItems>
    implements ExchangeItemsService{

}




