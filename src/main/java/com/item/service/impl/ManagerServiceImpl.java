package com.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.item.pojo.Manager;
import com.item.service.ManagerService;
import com.item.mapper.ManagerMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author XXDLD
* @description 针对表【manager】的数据库操作Service实现
* @createDate 2024-05-25 20:51:19
*/
@Service
public class ManagerServiceImpl extends ServiceImpl<ManagerMapper, Manager>
    implements ManagerService{


    @Override
    public List<Manager> getManagerStatusByManagerName() {
        return baseMapper.getManagerStatusByManagerName();
    }

    @Override
    public Boolean updateOnByManagerName(String managerName) {
        return baseMapper.updateOnByManagerName(managerName);
    }

    @Override
    public Boolean updateOffByManagerName(String managerName) {
        return baseMapper.updateOffByManagerName(managerName);
    }
}




