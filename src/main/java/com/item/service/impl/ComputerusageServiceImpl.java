package com.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.item.mapper.ComputerMapper;
import com.item.pojo.ComputerDailyProfit;
import com.item.pojo.Computerusage;
import com.item.service.ComputerusageService;
import com.item.mapper.ComputerusageMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import com.item.mapper.usermapper;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* @author likaiping
* @description 针对表【computerusage】的数据库操作Service实现
* @createDate 2024-04-21 12:11:27
*/
@Service
@AllArgsConstructor
public class ComputerusageServiceImpl extends ServiceImpl<ComputerusageMapper, Computerusage>
    implements ComputerusageService{
    private ComputerusageMapper computerusageMapper;
    private ComputerMapper computerMapper;
    private usermapper usermapper;
    @Override
    public List<Map<String, String>> findComputerUsageListByIsDelete(Integer isDelete) {
        List<Map<String, String>> list = new ArrayList<>();
        List<Computerusage> computerusageList = computerusageMapper.selectComputerUsageListByIsDelete(isDelete);
        for (Computerusage computerusage : computerusageList) {
            Map<String, String> map = new HashMap<>();
            String detail = computerMapper.selectDetailById(computerusage.getComputerId());
            String username = usermapper.selectUsernameById(computerusage.getUserId());
            LocalDateTime starttime = computerusage.getStarttime();
            BigDecimal balance = computerMapper.selectBalanceByUsername(username);
            map.put("detail",detail);
            map.put("username",username);
            map.put("starttime",starttime.toString());
            map.put("state","正在使用");
            map.put("balance",balance.toString());
            list.add(map);
        }
        return list;
    }

    @Override
    public List<Computerusage> findComputerUsageAllList(String username, String firstTime, String secondTime) {
        List<Computerusage> list = null;
        //如果为空则默认查询所有信息
        if (username.isEmpty() && firstTime.isEmpty() ) {
            list = computerusageMapper.selectComputerUsageAllList();
        }else {
            Long userId = usermapper.findIdByUsername(username);
            list = computerusageMapper.selectHistoryByTimeToTime(userId, firstTime, secondTime);
        }
        return list;
    }

    @Override
    public List<ComputerDailyProfit> getComputerDailyProfit() {
        return computerusageMapper.selectDailyProfit();
    }

    @Override
    public ComputerDailyProfit getTodayProfit() {
        String todayDate = LocalDate.now().toString();
        return computerusageMapper.selectTodayProfit(todayDate);
    }

    @Override
    public ComputerDailyProfit getYesterdayProfit() {
        // 获取当前日期
        LocalDate today = LocalDate.now();
        // 获取昨天的日期
        LocalDate yesterday = today.minusDays(1);
        // 将昨天的日期转换为字符串格式
        String yesterdayDate = yesterday.toString();
        return computerusageMapper.selectTodayProfit(yesterdayDate);
    }
}




