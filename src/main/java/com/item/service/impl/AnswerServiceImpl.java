package com.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.item.pojo.Answer;
import com.item.service.AnswerService;
import com.item.mapper.AnswerMapper;
import org.springframework.stereotype.Service;

/**
* @author XXDLD
* @description 针对表【answer】的数据库操作Service实现
* @createDate 2024-05-17 15:23:50
*/
@Service
public class AnswerServiceImpl extends ServiceImpl<AnswerMapper, Answer>
    implements AnswerService{

}




