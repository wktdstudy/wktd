package com.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.item.mapper.usermapper;
import com.item.pojo.RechargeHistory;
import com.item.pojo.RechargeRecord;
import com.item.pojo.Result;
import com.item.service.RechargeRecordService;
import com.item.mapper.RechargeRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
* @author XXDLD
* @description 针对表【recharge_record】的数据库操作Service实现
* @createDate 2024-04-24 20:39:46
*/
@Service
public class RechargeRecordServiceImpl extends ServiceImpl<RechargeRecordMapper, RechargeRecord>
    implements RechargeRecordService{

    @Autowired
    private  usermapper usermapper;
    @Autowired
    private RechargeRecordMapper rechargeRecordMapper;

    @Override
    public boolean insertRechargeRecord(Long tradeId,double record, String username) {
        int idByUsername = Math.toIntExact(usermapper.findIdByUsername(username));

        BigDecimal balanceByUsername = usermapper.getBalanceByUsername(username);

        String status = "成功";

        return rechargeRecordMapper.insertRechargeRecord(tradeId,idByUsername,record,balanceByUsername,status);
    }

    @Override
    public List<RechargeRecord> getRechargeRecordByUsersId(String username) {
        Long idByUsername = usermapper.findIdByUsername(username);
        return rechargeRecordMapper.getRechargeRecordByUsersId(Math.toIntExact(idByUsername));
    }

    @Override
    public List<RechargeHistory> getRechargeHistory() {
        return baseMapper.getRechargeHistory();
    }

}




