package com.item.service;


import com.item.pojo.User;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface userservice {
    User getusername(String username);

    void register(String username, String password, int age, String address, String sex, String phone, String idCard, String email);

    BigDecimal getBalanceByUsername(String username);

    void updateBalanceByUsersId(double record, String username);

    void update(String password, Integer age, String userid);

    List<User> getPointsByUsername(String username);

    Boolean dailyCheckIn(String username);

    LocalDate getCheckinDateByUsername(String username);

    Boolean updateCheckinDateByUsername(String username, LocalDate today);

    List<User> getUserInfo(String username);

    Boolean updateUserInfo(String username, String email, Integer age, String phone, String idCard, String address, String sex);


    Boolean updateUserAvatar(String username, String imageUrl);
}
