package com.item.service;

import com.item.pojo.ExchangeItems;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author XXDLD
* @description 针对表【exchange_items】的数据库操作Service
* @createDate 2024-06-05 12:23:09
*/
public interface ExchangeItemsService extends IService<ExchangeItems> {

}
