package com.item.service;

import com.item.pojo.Manager;
import com.baomidou.mybatisplus.extension.service.IService;
import com.item.service.impl.ManagerServiceImpl;

import java.util.List;

/**
* @author XXDLD
* @description 针对表【manager】的数据库操作Service
* @createDate 2024-05-25 20:51:19
*/
public interface ManagerService extends IService<Manager> {
    List<Manager> getManagerStatusByManagerName();

    Boolean updateOnByManagerName(String managerName);

    Boolean updateOffByManagerName(String managerName);
}
