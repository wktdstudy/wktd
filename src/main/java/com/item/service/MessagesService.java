package com.item.service;

import com.item.pojo.Messages;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author XXDLD
* @description 针对表【messages】的数据库操作Service
* @createDate 2024-05-30 15:34:45
*/
public interface MessagesService extends IService<Messages> {
    Boolean putMessagesByuserName(String fromUserName, String text, String type, String toUserName);

    List<Messages> getMessagesByUserNames(String fromUserName, String toUserName);
}
