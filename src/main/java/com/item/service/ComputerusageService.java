package com.item.service;

import com.item.pojo.ComputerDailyProfit;
import com.item.pojo.Computerusage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
* @author likaiping
* @description 针对表【computerusage】的数据库操作Service
* @createDate 2024-04-21 12:11:27
*/
public interface ComputerusageService extends IService<Computerusage> {

    /**
     * 根据is_delete查询正在上机的电脑
     * @param isDelete 是否正在上机的值 0标识正在上机
     * @return 正在上机的电脑信息list
     */
    List<Map<String, String>> findComputerUsageListByIsDelete(Integer isDelete);

    /**
     * 管理员根据条件查询所有人上机历史
     * @param username 所需要查询的用户名
     * @param firstTime 开始时间
     * @param secondTime 结束时间
     * @return 上机历史记录列表
     */
    List<Computerusage> findComputerUsageAllList(String username, String firstTime, String secondTime);

    /**
     * 获取每日收益
     * @return 每日收益
     */
    List<ComputerDailyProfit> getComputerDailyProfit();

    /**
     * 获取今日收益
     * @return 今日收益
     */
    ComputerDailyProfit getTodayProfit();

    /**
     * 获取昨日收益
     * @return 昨日收益
     */
    ComputerDailyProfit getYesterdayProfit();

}
