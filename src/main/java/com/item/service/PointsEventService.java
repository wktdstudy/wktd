package com.item.service;

import com.item.pojo.*;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
* @author XXDLD
* @description 针对表【points_event】的数据库操作Service
* @createDate 2024-06-04 10:26:18
*/
public interface PointsEventService extends IService<PointsEvent> {

    void insertPointsEvent(String username);

    List<PointsEvent> getPointsEvent(String username);

    String pointsToBalance(String username, Integer pointsNumber, BigDecimal balance);

    List<ExchangeItems> getExchangeItems();

    List<Coupons> getCoupons();

    List<LotteryItems> getLotteryItems();

    String exchangeItemsCoupons(String username, Integer couponsId, Integer pointsNumber);

    String exchangeItemsType1(String username,Integer pointsNumber);

    String exchangeItemsType0(String username, Integer couponsId, Integer pointsNumber);

    String lotteryDraw(String username, Integer pointsValue);

    List<LotteryRecords> getLotteryRecords();

    //管理部分
    Boolean changeLotteryItem(LotteryItems lotteryItems);

    boolean addLotteryItem(LotteryItems lotteryItems);

    boolean deleteLotteryItem(Integer lottertyItemId);

    boolean changeExchangeItems(ExchangeItems exchangeItems);

    boolean changeCoupons(Integer id, Integer price);

    boolean addExchangeItems(ExchangeItems exchangeItems);

    boolean deleteExchangeItems(Integer exchangeItemsId);

    boolean deleteCoupons(Integer id);

    List<Map<String,String>> getExchangeItemsRecords();

    List<Map<String,String>> getCouponsRecords();

    List<PointsEvent> getAllPointsEvent();
}
