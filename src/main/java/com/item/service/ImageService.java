package com.item.service;

import com.item.pojo.Image;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author likaiping
* @description 针对表【image】的数据库操作Service
* @createDate 2024-06-09 15:18:50
*/
public interface ImageService extends IService<Image> {

    /**
     * 根据类型查询图片
     * @param type 图片类型
     * @return 图片列表
     */
    List<Image> findImageListByType(String type);

    /**
     * 查询所有图片
     * @return 图片列表
     */
    List<Image> findAllImage();

    /**
     * 禁用图片
     * @param imgName 图片名
     * @return 禁用结果
     */
    boolean forbiddenImage(String imgName);

    /**
     * 解禁图片
     * @param imgName 图片名
     * @return 解禁结果
     */
    boolean unForbiddenImage(String imgName);

    /**
     * 添加图片
     * @param name 图片名称
     * @param imgUrl 图片路径
     * @return 插入结果
     */
    boolean addImage(String name,String imgUrl);

    /**
     * 编辑图片
     * @param image 图片
     * @return 修改结果
     */
    boolean editImage(Image image);
}
