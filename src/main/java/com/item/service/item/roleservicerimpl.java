package com.item.service.item;
;
import com.item.mapper.AuthMapper;
import com.item.mapper.RoleMapper;
import com.item.mapper.springsecuritymapper;
import com.item.pojo.Auth;
import com.item.pojo.Role;
import com.item.pojo.User;
import com.item.service.roleservice;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@AllArgsConstructor
public class roleservicerimpl  implements roleservice {

    private springsecuritymapper springsecuritymapper;
    private RoleMapper roleMapper;
    private AuthMapper authMapper;

    @Override
    public List<Role> findrolebyusername(String username) {
        List<Role> roles= springsecuritymapper.findByUsername(username);
        return roles;
    }

    @Override
    public User getuser(String username) {
      User user=  springsecuritymapper.getusername(username);

        return user;
    }

    @Override
    public List<Auth> findauthbyusername(String username) {
        List<Auth> auth= springsecuritymapper.findAuthByUsername(username);
        return auth;
    }

    @Override
    public List<Role> findAllRoleList() {
        return roleMapper.selectRoleList();
    }

    @Override
    public boolean addRole(String roleName, String createUsername) {
        return roleMapper.insertRole(roleName,createUsername);
    }

    @Override
    public boolean removeRoleByRoleName(String roleName) {
        Long roleId = roleMapper.selectRoleIdByRoleName(roleName);
        return roleMapper.deleteRoleByRoleId(roleId);
    }

    @Override
    public boolean editRoleName(String roleName, String newName, String updateUsername) {
        Long roleId = roleMapper.selectRoleIdByRoleName(roleName);
        return roleMapper.updateRoleNameByRoleId(roleId,newName,updateUsername);
    }

    @Override
    public List<Auth> getRoleParentAuthList(String roleName) {
        Long roleId = roleMapper.selectRoleIdByRoleName(roleName);
        if (roleId != null) {
            List<Auth> parentAuthList = roleMapper.selectParentAuthListByRoleId(roleId);
            return parentAuthList;
        }else {
            throw new RuntimeException("角色不存在");
        }
    }

    @Override
    public List<Auth> getRoleAuthList(String roleName) {
        Long roleId = roleMapper.selectRoleIdByRoleName(roleName);
        if (roleId != null) {
            return roleMapper.selectAuthListByRoleId(roleId);
        }
        return null;
    }

    @Override
    public boolean addAuthsToRole(String roleName, String authPidName) {
        Long roleId = roleMapper.selectRoleIdByRoleName(roleName);
        Long pid = authMapper.selectIdByPidName(authPidName);
        Long authIdInRoleAuth = roleMapper.isAuthIdInRoleAuth(roleId, pid);
        if (authIdInRoleAuth != null) {
            //说明该父权限已经加入到该角色当中
            throw new RuntimeException("该角色已拥有该权限");
        }
        if (pid != null) {
            List<Long> authIds = authMapper.listAuthIdsByPid(pid);
            //把父权限也加入进去
            authIds.add(pid);
            return roleMapper.insertAuthsToRole(roleId,authIds);
        }
        return false;
    }

    @Override
    public boolean removeAuthsGroupOfRole(String roleName, String authPidName) {
        Long roleId = roleMapper.selectRoleIdByRoleName(roleName);
        if (roleId != null) {
            Long pid = authMapper.selectIdByPidName(authPidName);
            if (pid != null) {
                List<Long> authIds = authMapper.listAuthIdsByPid(pid);
                authIds.add(pid);
                return roleMapper.deleteAuthsGroupOfRole(roleId,authIds);
            }
        }
        return false;
    }

    @Override
    public boolean removeAllAuthsOfRole(String roleName) {
        Long roleId = roleMapper.selectRoleIdByRoleName(roleName);
        if (roleId != null) {
            return roleMapper.deleteRoleAllAuthByRoleId(roleId);
        }
        return false;
    }

    @Override
    public boolean addAllAuthsToRole(String roleName) {
        Long roleId = roleMapper.selectRoleIdByRoleName(roleName);
        if (roleId != null) {
            //在添加所有权限之前先删除该角色的所有权限
            roleMapper.deleteRoleAllAuthByRoleId(roleId);

            List<Long> allAuthIds = authMapper.selectAllAuthId();
            return roleMapper.insertAuthsToRole(roleId, allAuthIds);
        }
        return false;
    }

    @Override
    public boolean forbiddenRole(String roleName) {
        Long roleId = roleMapper.selectRoleIdByRoleName(roleName);
        if (roleId != null) {
            return roleMapper.forbiddenRole(roleId);
        }
        return false;
    }

    @Override
    public boolean unForbiddenRole(String roleName) {
        Long roleId = roleMapper.selectRoleIdByRoleName(roleName);
        if (roleId != null) {
            return roleMapper.unForbiddenRole(roleId);
        }
        return false;
    }
}
