package com.item.service.item;

import com.item.utils.RedisUtil;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class GetridismessageService {
    private RedisUtil redisUtil;
    private RedisTemplate<String, Object> redisTemplate;
    public String getmessage()
    {
        String userstringid= (String) redisTemplate.opsForValue().get("userid");
        return (String) redisUtil.hashGet("usermessage",userstringid) ;
    }
    public String getitemmessage()
    {

        return (String) redisUtil.hashGet("itemmessage", String.valueOf(1)) ;
    }
    public String getcouponmessage()
    {
        String userstringid= (String) redisTemplate.opsForValue().get("userid");
        return (String) redisUtil.hashGet("couponmessage", userstringid) ;
    }
}
