package com.item.service.item;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MessagePublisherService {


    private RedisTemplate<String, Object> redisTemplate;

    public void publishToCommon(String message) {
        redisTemplate.convertAndSend("common:topic", message);

    }
    public void publishToItem(String message) {
        redisTemplate.convertAndSend("orther:topic", message);

    }
    public void publishTocoupon(String message) {
        redisTemplate.convertAndSend("coupon:topic", message);
    }
}
