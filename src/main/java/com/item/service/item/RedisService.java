package com.item.service.item;

import com.item.utils.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class RedisService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;
@Autowired

    private JavaMailSender javaMailSender;
    public String saveVerificationCode(String email, String code) {
        String title="这是一段验证码";
        String code1="这是一段验证码功能"+code;
        EmailService emailService=new EmailService();
        if (javaMailSender != null) {

            System.out.println("JavaMailSender is not null");
        } else {

            System.out.println("JavaMailSender is null");
        }

        redisTemplate.opsForValue().set(email, code);
       emailService.sendEmail(email,title,code1);
        System.out.println(email+code);
        // 设置过期时间为5分钟
        redisTemplate.expire(email, 5, TimeUnit.MINUTES);//邮箱，时间，时间类型
        return "true";
    }

    public String getVerificationCode(String email) {
        return redisTemplate.opsForValue().get(email);
    }

    public void deleteVerificationCode(String email) {
        redisTemplate.delete(email);
    }
}
