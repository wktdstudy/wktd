package com.item.service.item;

import com.item.mapper.usermapper;
import com.item.pojo.User;
import com.item.service.userservice;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class userserviceitem implements userservice {

    private usermapper usermapper;


    @Override
    public User getusername(String username) {
        return usermapper.getusername(username);
    }

    @Override
    public void register(String username, String password, int age, String address, String sex, String phone, String idCard, String email)
    {
        usermapper.register(username,password,age,address,sex,phone,idCard,email);
        //给新用户赋予用户角色
        Long userId = usermapper.findIdByUsername(username);
        Long roleId = usermapper.selectRoleIdByRoleName("用户");
        if (usermapper.insertRoleToNewUser(userId,roleId)) {
            System.out.println("新注册用户赋予权限失败");
        }
    }

    @Override
    public BigDecimal getBalanceByUsername(String username) {
        return usermapper.getBalanceByUsername(username);
    }

    @Override
    public void updateBalanceByUsersId(double record, String username) {
        int idByUsername = Math.toIntExact(usermapper.findIdByUsername(username));
        usermapper.updateBalanceById(idByUsername, record);
    }

    @Override
    public void update(String password, Integer age, String userid) {
        usermapper.updatedetails(password,age,userid);
    }

    @Override
    public List<User> getPointsByUsername(String username) {
        return usermapper.getPointsByUsername(username);
    }

    @Override
    public Boolean dailyCheckIn(String username) {
        return usermapper.dailyCheckIn(username);
    }

    @Override
    public LocalDate getCheckinDateByUsername(String username) {
        return usermapper.getCheckinDateByUsername(username);
    }

    @Override
    public Boolean updateCheckinDateByUsername(String username, LocalDate today) {
        return usermapper.updateCheckinDateByUsername(username,today);
    }

    @Override
    public List<User> getUserInfo(String username) {
        return usermapper.getUserInfo(username);
    }

    @Override
    public Boolean updateUserInfo(String username, String email, Integer age, String phone, String idCard, String address, String sex) {
        System.out.println("aaa"+username+email+age+phone+idCard+address+sex);
        // 获取当前时间
        LocalDateTime now = LocalDateTime.now();
        return usermapper.updateUserInfo(username,email,age,phone,idCard,address,now,sex);
    }

    @Override
    public Boolean updateUserAvatar(String username, String imageUrl) {
        return usermapper.updateUserAvatar(username,imageUrl);
    }

}
