package com.item.mapper;

import com.item.pojo.Manager;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
* @author XXDLD
* @description 针对表【manager】的数据库操作Mapper
* @createDate 2024-05-25 20:51:19
* @Entity com.item.pojo.Manager
*/
public interface ManagerMapper extends BaseMapper<Manager> {

    @Select("SELECT manager_name,status FROM manager")
    List<Manager> getManagerStatusByManagerName();

    @Update("UPDATE manager SET status=1 WHERE manager_name=#{managerName}")
    Boolean updateOnByManagerName(@Param("managerName") String managerName);

    @Update("UPDATE manager SET status=0 WHERE manager_name=#{managerName}")
    Boolean updateOffByManagerName(@Param("managerName") String managerName);
}




