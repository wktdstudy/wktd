package com.item.mapper;

import com.item.pojo.Messages;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;

/**
* @author XXDLD
* @description 针对表【messages】的数据库操作Mapper
* @createDate 2024-05-30 15:34:45
* @Entity com.item.pojo.Messages
*/
public interface MessagesMapper extends BaseMapper<Messages> {
    @Insert("INSERT INTO historical_messages(from_user_name,messages_text,messages_type,to_user_name) " +
            "VALUES(#{fromUserName},#{messagesText},#{messagesType},#{toUserName})")
    Boolean putMessagesByuserName(String fromUserName, String messagesText, String messagesType, String toUserName);

    @Select("\n" +
            "SELECT * \n" +
            "FROM historical_messages\n" +
            "WHERE (from_user_name = #{fromUserName} AND to_user_name = #{toUserName})\n" +
            "   OR (from_user_name = #{toUserName} AND to_user_name = #{fromUserName});")
     List<Messages> getMessagesByUserNames(@Param("fromUserName") String fromUserName, @Param("toUserName") String toUserName);
}




