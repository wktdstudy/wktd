package com.item.mapper;

import com.item.pojo.LotteryItems;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author XXDLD
* @description 针对表【lottery_items】的数据库操作Mapper
* @createDate 2024-06-05 15:30:19
* @Entity com.item.pojo.LotteryItems
*/
public interface LotteryItemsMapper extends BaseMapper<LotteryItems> {

}




