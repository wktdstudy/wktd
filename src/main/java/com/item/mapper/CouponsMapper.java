package com.item.mapper;

import com.item.pojo.Coupons;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author XXDLD
* @description 针对表【coupons】的数据库操作Mapper
* @createDate 2024-06-05 12:22:44
* @Entity com.item.pojo.Coupons
*/
public interface CouponsMapper extends BaseMapper<Coupons> {

}




