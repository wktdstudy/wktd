package com.item.mapper;

import com.item.pojo.ExchangeItems;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author XXDLD
* @description 针对表【exchange_items】的数据库操作Mapper
* @createDate 2024-06-05 12:23:09
* @Entity com.item.pojo.ExchangeItems
*/
public interface ExchangeItemsMapper extends BaseMapper<ExchangeItems> {

}




