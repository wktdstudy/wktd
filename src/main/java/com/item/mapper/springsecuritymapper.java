package com.item.mapper;

import com.item.pojo.Auth;
import com.item.pojo.Role;
import com.item.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper
public interface springsecuritymapper {
    @Select("SELECT * FROM role WHERE is_deleted=0 and id IN (SELECT role_id FROM user_role WHERE user_id IN (SELECT id FROM users WHERE username = #{username}))")
    List<Role> findByUsername(@Param("username") String username);

    @Select("SELECT * FROM auth " +
            "WHERE is_deleted=0 and id IN (" +
            "    SELECT auth_id " +
            "    FROM role_auth " +
            "    WHERE role_id IN (" +
            "        SELECT id " +
            "        FROM role " +
            "        WHERE id IN (" +
            "            SELECT role_id " +
            "            FROM user_role " +
            "            WHERE user_id IN (" +
            "                SELECT id " +
            "                FROM users " +
            "                WHERE username = #{username}" +
            "            )" +
            "        )" +
            "    )" +
            ")")
    List<Auth> findAuthByUsername(@Param("username") String username);

    @Select("select * from users where username=#{username} and banned=0")
    User getusername(@Param("username") String username);
}



