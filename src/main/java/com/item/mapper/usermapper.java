package com.item.mapper;

import com.item.pojo.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Mapper
@Component
public interface usermapper {

    @Select("select * from users where username=#{username}")
    User getusername(String username);

    @Insert("INSERT INTO users(username, password, age, address, sex, phone, id_card, email, create_time, update_time) " +
            "VALUES (#{username}, #{password}, #{age}, #{address}, #{sex}, #{phone}, #{idCard}, #{email}, now(), now())")
    void register(@Param("username") String username,
                  @Param("password") String password,
                  @Param("age") int age,
                  @Param("address") String address,
                  @Param("sex") String sex,
                  @Param("phone") String phone,
                  @Param("idCard") String idCard,
                  @Param("email") String email);

    /**
     * 注册用户时给新用户新增用户角色
     * @param userId 新用户id
     * @param roleId 角色id
     * @return
     */
    @Insert("INSERT INTO user_role(user_id, role_id) VALUES (#{userId}, #{roleId})")
    boolean insertRoleToNewUser(@Param("userId") Long userId, @Param("roleId") Long roleId);

    /**
     * 根据角色名查询角色id
     * @param roleName 角色名
     * @return 角色id
     */
    @Select("select id from role where rolename=#{roleName}")
    Long selectRoleIdByRoleName(@Param("roleName") String roleName);

    /**
     * 根据用户名查询用户id
     * @param username 用户名
     * @return 用户id
     */
    @Select("select id from users where username=#{username}")
        Long findIdByUsername(@Param("username") String username);

    /**
     * 根据用户名查询用户余额
     * @param username 用户名
     * @return 用户余额
     */
    @Select("SELECT balance FROM users WHERE username=#{username}")
    BigDecimal getBalanceByUsername(@Param("username") String username);

    /**
     * 根据用户ID更新用户余额（balance）
     * @param id
     * @param record
     * @return
     */
    @Update("UPDATE users\n" +
            "SET balance = balance + #{record}\n" +
            "WHERE id=#{id}")
    Boolean updateBalanceById(@Param("id") int id,@Param("record") double record);

    /**
     * 根据用户ID更新用户的密码和年龄信息。
     * @param password 用户的新密码。
     * @param age 用户的新年龄。
     * @param userid 用户的ID，用于定位要更新的记录。
     */
    @Update("update users set password=#{password},age=#{age} where id =#{userid}")
    void updatedetails(@Param("password") String password, Integer age, String userid);

    /**
     * 获取用户的积分
     * @param username
     * @return
     */
    @Select("SELECT points,last_checkin_date FROM users WHERE username=#{username}")
    List<User> getPointsByUsername(String username);

    /**
     * 通过用户名获取上次签到日期。
     *
     * @param username 用户名，用于查询用户信息。
     * @return 上次签到的LocalDate日期。
     */
    @Select("SELECT last_checkin_date FROM users WHERE username=#{username}")
    LocalDate getCheckinDateByUsername(String username);

    /**
     * 通过用户名更新签到日期。
     *
     * @param username 用户名，用于定位要更新的用户。
     * @param lastCheckinDate 新的签到日期，用于更新用户的签到信息。
     * @return 更新操作的成功与否。
     */
    @Update("UPDATE users SET last_checkin_date=#{lastCheckinDate} WHERE username=#{username}")
    Boolean updateCheckinDateByUsername(String username,LocalDate lastCheckinDate);

    /**
     * 用户每日签到，增加50积分。
     *
     * @param username 用户名，用于定位要进行签到操作的用户。
     * @return 签到操作的成功与否。
     */
    @Update("UPDATE users SET points=points+50 WHERE username=#{username};")
    Boolean dailyCheckIn(@Param("username") String username);

    /**
     * 从用户的积分中扣除指定数量的积分。
     *
     * @param username 用户名，作为查询用户的条件。
     * @param pointsNumber 需要从用户积分中扣除的数量。
     * @return Boolean 返回操作是否成功的标志。成功更新用户积分返回true，否则返回false。
     */
    @Update("UPDATE users SET points=points-#{pointsNumber} WHERE username=#{username}")
    Boolean takeOutPoints(@Param("username") String username,@Param("pointsNumber") Integer pointsNumber);

    /**
     * 根据用户ID查询用户名
     * @param id 用户ID
     * @return 用户名
     */
    @Select("select username from users where id=#{id}")
    String selectUsernameById(@Param("id") Integer id);

    @Select("select * from users where username=#{username}")
    List<User> getUserInfo(String username);

    @Update("update users set email=#{email},age=#{age},phone=#{phone},id_card=#{idCard},address=#{address},update_time=#{now},sex=#{sex} where username=#{username}")
    Boolean updateUserInfo(String username, String email, Integer age, String phone, String idCard, String address, LocalDateTime now, String sex);

    @Update("update users set imageurl=#{imageUrl} where username=#{username}")
    Boolean updateUserAvatar(String username, String imageUrl);
}





