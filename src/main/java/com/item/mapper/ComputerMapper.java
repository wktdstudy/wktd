package com.item.mapper;

import com.item.pojo.Computer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.item.pojo.Computerusage;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
* @author likaiping
* @description 针对表【computer】的数据库操作Mapper
* @createDate 2024-04-21 12:11:27
* @Entity com.item.pojo.Computer
*/
public interface ComputerMapper extends BaseMapper<Computer> {

    /**
     * 模糊查询电脑列表 筛选
     * @param price 价格
     * @param detail 电脑类型
     * @return computer的list集合
     */
    @Select("<script>" +
            "select * from computer where 1=1" +
            "<if test=\"price!=null\"> and price = #{price} </if>" +
            "<if test=\"detail!=null\"> and detail LIKE CONCAT('%',#{detail},'%') </if>" +
            "</script>")
    List<Computer> findListSearchComputer(@Param("price") BigDecimal price, @Param("detail") String detail);

    /**
     * 通过电脑机型detail查询电脑id
     * @param detail 电脑机型
     * @return 电脑id
     */
    @Select("select id from computer where detail=#{detail}")
    Long findIdByDetail(@Param("detail") String detail);

    /**
     * 根据电脑机型查询价格/单价
     * @param detail 电脑机型
     * @return 单价price
     */
    @Select("select price from computer where  detail=#{detail}")
    BigDecimal findPriceByDetail(@Param("detail") String detail);



    /**
     * 根据用户名查询用户的余额
     * @param username
     * @return
     */
    @Select("select balance from users where username=#{username}")
    BigDecimal selectBalanceByUsername(@Param("username") String username);

    /**
     * 根据用户名和当前花费，修改用户的余额
     * @param currentCost 当前花费
     * @param username 用户名
     * @return 布尔值
     */
    @Update("UPDATE users SET balance=balance-#{currentCost} where username=#{username}")
    boolean updateBalanceByUsername(@Param("currentCost") BigDecimal currentCost ,@Param("username") String username);

    /**
     * 根据用户名修改用户的积分
     * @param points 积分
     * @param username 用户名
     * @return 布尔值
     */
    @Update("UPDATE users SET points=points+#{points} where username=#{username}")
    boolean updatePointsByUsername(@Param("points") Integer points, @Param("username") String username);

    /**
     * 添加电脑
     * @param detail 电脑机型
     * @param price 电脑价格
     * @return 布尔值
     */
    @Insert("insert into computer(price,detail) values(#{price},#{detail})")
    boolean insertComputer(@Param("detail") String detail, @Param("price") BigDecimal price);

    /**
     * 根据电脑机型删除电脑
     * @param id 电脑id
     * @return
     */
    @Delete("delete from computer where id=#{id}")
    boolean deleteByDetailBoolean(@Param("id") Integer id);

    /**
     * 根据电脑机型修改电脑信息
     * @param detail
     * @param price
     * @return
     */
    /*@Update("<script>" +
            "update computer set 1=1" +
            "<if test=\"price!=null\"> and price = #{price}</if>" +
            "<if test=\"detail!=null\"> and detail = #{detail}</if>" +
            "where detail=#{detail}" +
            "</script>")*/
    @Update("update computer set price=#{price} where detail=#{detail}")
    boolean updateComputerPrice(@Param("detail") String detail, @Param("price") BigDecimal price);

    /**
     * 根据电脑机型查询电脑id
     * @param detail
     * @return
     */
    @Select("select id from computer where detail=#{detail}")
    Integer selectIdByDetail(@Param("detail") String detail);

    /**
     * 根据电脑id修改电脑信息
     * @param detail 电脑机型
     * @param price 价格
     * @param id 电脑id
     * @return 布尔值
     */
    @Update("UPDATE computer set detail=#{detail}, price=#{price} where id=#{id}")
    boolean updateComputer(@Param("detail") String detail, @Param("price") BigDecimal price, @Param("id") Integer id);

    /**
     * 根据电脑id查询电脑机型
     * @param id 电脑id
     * @return 电脑机型
     */
    @Select("select detail from computer where id=#{id}")
    String selectDetailById(@Param("id") Integer id);
}




