package com.item.mapper;

import com.item.pojo.*;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * @author XXDLD
 * @description 针对表【points_event】的数据库操作Mapper
 * @createDate 2024-06-04 10:26:18
 * @Entity com.item.pojo.PointsEvent
 */
public interface PointsEventMapper extends BaseMapper<PointsEvent> {

    @Insert("INSERT INTO points_event (username,event,event_time,points) VALUES(#{username},#{event},#{eventTime},#{points})")
    Boolean insertPointsEvent(String username, String event, String eventTime, String points);

    @Select("SELECT * FROM points_event WHERE username=#{username}")
    List<PointsEvent> getPointsEvent(String username);

    @Select("SELECT * FROM exchange_items")
    List<ExchangeItems> getExchangeItems();

    @Select("SELECT * FROM coupons WHERE status=1")
    List<Coupons> getCoupons();

    @Select("SELECT * FROM lottery_items")
    List<LotteryItems> getLotteryItems();

    @Insert("INSERT INTO users_coupons (users_id,coupons_id) VALUES (#{usersId},#{couponsId})")
    Boolean insertCouponsToUsers(Integer usersId, Integer couponsId);

    @Select("SELECT coupons_id FROM users_coupons WHERE users_id=#{usersId}")
    List<Integer> getCouponsIdByUsersId(@Param("usersId") Integer usersId);

    @Select("SELECT exchange_items_id FROM user_exchange_items WHERE user_id=#{idByUsername}")
    List<Integer> getExchangeItemIdByUsersId(@Param("idByUsername") int idByUsername);

    @Insert("INSERT INTO user_exchange_items (user_id,exchange_items_id) VALUES (#{idByUsername},#{couponsId})")
    void insertExchangeIdToUsers(@Param("idByUsername") int idByUsername, @Param("couponsId") Integer couponsId);

    @Insert("INSERT INTO users_lottery_items (users_id,lottery_items_id,lottery_items_title,date) VALUES (#{idByUsername},#{lotteryItemsId},#{lotteryItemsTitle},#{date})")
    Boolean insertUserLotteryRecord(@Param("idByUsername") int idByUsername, @Param("lotteryItemsId") int lotteryItemsId, @Param("lotteryItemsTitle") String lotteryItemsTitle, @Param("date") LocalDate date);

    @Select("SELECT COUNT(users_id) FROM users_lottery_items WHERE users_id=#{idByUsername} AND date=#{date}")
    int getUserDrawTimes(@Param("idByUsername") int idByUsername,@Param("date") LocalDate date);

    @Update("UPDATE users SET points=points+#{value} WHERE id=#{idByUsername}")
    void updatePoints(@Param("idByUsername") int idByUsername, @Param("value") int value);

    @Update("UPDATE lottery_items SET status=0 WHERE lottery_items_id=#{lotteryItemsId}")
    void updateStatusById(@Param("lotteryItemsId") Integer lotteryItemsId);

    @Update("UPDATE lottery_items SET number=number-1 WHERE lottery_items_id=#{lotteryItemsId}")
    void updateNumberById(@Param("lotteryItemsId") int lotteryItemsId);

    @Select("SELECT users.username, users_lottery_items.lottery_items_title,users_lottery_items.date\n" +
            "FROM users_lottery_items \n" +
            "LEFT JOIN users ON users.id = users_lottery_items.users_id;\n")
    List<LotteryRecords> getLotteryRecords();

    @Update("<script>" +
            "UPDATE lottery_items " +
            "<set>" +
            "<if test='title != null and title != \"\"'> title = #{title}, </if>" +
            "<if test='probability != null and probability != \"\"'> probability = #{probability}, </if>" +
            "<if test='description != null and description != \"\"'> description = #{description}, </if>" +
            "<if test='value != null and value != \"\"'> value = #{value}, </if>" +
            "<if test='number != null and number != \"\"'> number = #{number}, </if>" +
            "<if test='type != null and type != \"\"'> type = #{type}, </if>" +
            "<if test='status != null'> status = #{status} </if>" +
            "</set>" +
            "WHERE lottery_items_id = #{lotteryItemsId}" +
            "</script>")
    Boolean changeLotteryItem(LotteryItems lotteryItems);


    @Insert("INSERT INTO lottery_items (lottery_items_id,title,description,value,probability,number,status,type) " +
            "VALUES (#{lotteryItemsId},#{title},#{description},#{value},#{probability},#{number},#{status},#{type})")
    boolean addLotteryItem(LotteryItems lotteryItems);

    @Delete("DELETE FROM lottery_items WHERE lottery_items_id=#{lottertyItemId}")
    boolean deleteLotteryItem(@Param("lottertyItemId") Integer lottertyItemId);


    @Update("<script>" +
            "UPDATE exchange_items " +
            "<set>" +
            "<if test='title != null and title != \"\"'> title = #{title}, </if>" +
            "<if test='description != null and description != \"\"'> description = #{description}, </if>" +
            "<if test='price != null and price != \"\"'> price = #{price}, </if>" +
            "<if test='type != null and type != \"\"'> type = #{type}, </if>" +
            "<if test='showdialog != null and showdialog != \"\"'> showdialog = #{showdialog}, </if>" +
            "</set>" +
            "WHERE exchange_items_id = #{exchangeItemsId}" +
            "</script>")
    boolean changeExchangeItems(ExchangeItems exchangeItems);


    @Update("UPDATE coupons SET price=#{price} WHERE id=#{id}")
    boolean changeCoupons(@Param("id") Integer id, @Param("price") Integer price);

    @Insert("INSERT INTO exchange_items (exchange_items_id,title,description,price,type,showdialog) " +
            "VALUES (#{exchangeItemsId},#{title},#{description},#{price},#{type},#{showdialog})")
    boolean addExchangeItems(ExchangeItems exchangeItems);

    @Delete("DELETE FROM exchange_items WHERE exchange_items_id=#{exchangeItemsId}")
    boolean deleteExchangeItems(@Param("exchangeItemsId") Integer exchangeItemsId);

    @Update("UPDATE coupons SET status = 0 WHERE id=#{id}")
    boolean deleteCoupons(@Param("id") Integer id);

    @Select("SELECT users.username,exchange_items.title\n" +
            "FROM user_exchange_items\n" +
            "LEFT JOIN users ON users.id=user_exchange_items.user_id\n" +
            "LEFT JOIN exchange_items ON exchange_items.exchange_items_id=user_exchange_items.exchange_items_id")
    List<Map<String,String>> getExchangeItemsRecords();

    @Select("SELECT users.username,coupons.title\n" +
            "FROM users_coupons\n" +
            "LEFT JOIN users ON users.id=users_coupons.users_id\n" +
            "LEFT JOIN coupons ON coupons.id=users_coupons.coupons_id ")
    List<Map<String,String>> getCouponsRecords();

    @Select("SELECT * FROM points_event")
    List<PointsEvent> getAllPointsEvent();
}




