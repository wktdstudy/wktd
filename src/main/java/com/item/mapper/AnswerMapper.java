package com.item.mapper;

import com.item.pojo.Answer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author XXDLD
* @description 针对表【answer】的数据库操作Mapper
* @createDate 2024-05-17 15:23:50
* @Entity com.item.pojo.Answer
*/
public interface AnswerMapper extends BaseMapper<Answer> {

}




