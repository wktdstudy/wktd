package com.item.mapper;

import com.item.pojo.ComputerDailyProfit;
import com.item.pojo.Computerusage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
* @author likaiping
* @description 针对表【computerusage】的数据库操作Mapper
* @createDate 2024-04-21 12:11:27
* @Entity com.item.pojo.Computerusage
*/
public interface ComputerusageMapper extends BaseMapper<Computerusage> {
    /**
     * 开始上机 插入用户id、开始时间、电脑id
     * @param userId 用户id
     * @param startTime 开始时间
     * @param computerId 电脑id
     * @return /
     */
    @Insert("insert into computerusage(user_id,starttime,computer_id) values(#{userId},#{startTime},#{computerId})")
    boolean insertComputerUsage(@Param("userId") Long userId, @Param("startTime") LocalDateTime startTime, @Param("computerId") Long computerId);

    /**
     * 根据用户id和电脑机型查询上机时间
     * @param userId 用户id
     * @param computerId 电脑id
     * @return 上机时间
     */
    @Select("select starttime from computerusage where user_id=#{userId} and computer_id=#{computerId} and is_delete=0")
    LocalDateTime findStartTimeByUserIdAndComputerId(@Param("userId") Long userId, @Param("computerId") Long computerId);

    /**
     * 根据用户id和开始时间查询整条电脑使用信息
     * @param userId 用户id
     * @param starttime 开始时间
     * @return /
     */
    @Select("select * from computerusage where user_id=#{userId} and starttime=#{starttime}")
    Computerusage findComputerUsageByUserIdAndStartTime(@Param("userId") Long userId, @Param("starttime") LocalDateTime starttime);

    /**
     * 根据用户id查询该用户历史上机记录
     * @param userId
     * @return /
     */
    @Select("select * from computerusage where user_id=#{userId} ORDER BY endtime DESC")
    List<Computerusage> findComputerUsageListByUserId(@Param("userId") Long userId);

    /**
     * 下机 更新computerusage表 更新结束时间 持续时间 费用
     * @param endTime 结束时间
     * @param duration 持续时间
     * @param cost 费用
     * @param userId 用户id
     * @param computerId 电脑id
     * @return /
     */
    @Update("update computerusage set endtime=#{endTime},duration=#{duration},cost=#{cost},is_delete=1 where user_id=#{userId} and computer_id=#{computerId} and is_delete=0")
    boolean updateComputerUsage(@Param("endTime")LocalDateTime endTime,
                                @Param("duration") double duration,
                                @Param("cost") BigDecimal cost,
                                @Param("userId") Long userId,
                                @Param("computerId") Long computerId
    );

    /**
     * 查询当前用户选择的电脑是否已经被该用户上机
     * @param computerId 电脑id
     * @return 0或者null
     */
    @Select("select is_delete from computerusage where computer_id=#{computerId} and user_id=#{userId} and is_delete=0")
    Integer selectComputerusageIsOpen(@Param("computerId") Long computerId, @Param("userId") Long userId);

    /**
     * 根据时间查询历史上机记录
     * @param userId 用户id
     * @param firstTime 第一个时间
     * @param secondTime 第二个时间
     * @return 历史记录信息
     */
//    @Select("select * from computerusage where user_id=#{userId} and starttime BETWEEN #{firstTime} and #{secondTime}")
    @Select("<script>" +
            "select * from computerusage where 1=1" +
            "<if test=\"userId!=null\"> and user_id=#{userId} </if>" +
            "<if test=\"firstTime!=null\"> and starttime BETWEEN #{firstTime} and #{secondTime} </if>" +
            "</script>")
    List<Computerusage> selectHistoryByTimeToTime(@Param("userId") Long userId, @Param("firstTime") String firstTime, @Param("secondTime") String secondTime);

    /**
     * 根据is_delete查询正在上机的电脑
     * @param isDelete 是否正在上机的值 0标识正在上机
     * @return 正在上机的电脑信息list
     */
    @Select("select * from computerusage where is_delete = #{isDelete}")
    List<Computerusage> selectComputerUsageListByIsDelete(@Param("isDelete") Integer isDelete);

    /**
     * 管理员查询所有用户的所有上机历史
     * @return  上机历史信息
     */
    @Select("select * from computerusage where is_delete=1 ORDER BY starttime DESC")
    List<Computerusage> selectComputerUsageAllList();

    /**
     * 查询电脑每日收益
     * @return 每日收益
     */
    @Select("SELECT DATE(`endtime`) AS date,SUM(`cost`) AS total_profit FROM computerusage GROUP BY DATE(`endtime`)")
    List<ComputerDailyProfit> selectDailyProfit();

    /**
     * 查询今日收益
     * @param todayDate 今天日期
     * @return
     */
    @Select("SELECT DATE_FORMAT(endtime, '%Y-%m-%d') AS date, SUM(cost) AS total_profit FROM computerusage WHERE DATE_FORMAT(endtime, '%Y-%m-%d') = #{todayDate}")
    ComputerDailyProfit selectTodayProfit(@Param("todayDate") String todayDate);

}




