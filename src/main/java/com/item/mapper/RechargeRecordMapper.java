package com.item.mapper;

import com.item.pojo.RechargeHistory;
import com.item.pojo.RechargeRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.item.pojo.Result;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
* @author XXDLD
* @description 针对表【recharge_record】的数据库操作Mapper
* @createDate 2024-04-24 20:39:46
* @Entity com.item.pojo.RechargeRecord
*/
public interface RechargeRecordMapper extends BaseMapper<RechargeRecord> {
    /**
     * 将充值记录插入数据库
     * @param id
     * @param record
     * @param balance
     * @param status
     * @return
     */
    @Insert("INSERT into recharge_record (id,user_id,recharge_time,balance_record,balance,status)\n" +
            "VALUES (#{tradeId},#{id},now(),#{record},#{balance},#{status})\n" +
            "       ")
    boolean insertRechargeRecord(@Param("tradeId") long tradeId,@Param("id") int id, @Param("record") double record, @Param("balance") BigDecimal balance, @Param("status") String status);

    /**
     * 根据id查询充值记录
     * @param id
     * @return
     */
    @Select("SELECT * FROM recharge_record WHERE user_id=#{id}")
    List<RechargeRecord> getRechargeRecordByUsersId(@Param("id") int id);

    @Select("SELECT users.username,recharge_record.*\n" +
            "FROM recharge_record\n" +
            "LEFT JOIN users ON users.id=recharge_record.user_id\n")
    List<RechargeHistory> getRechargeHistory();
}




