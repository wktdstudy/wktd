package com.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.item.pojo.Auth;
import com.item.pojo.Role;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author likaiping
 * @createTime 2024/6/4 18:11
 */
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 查询角色列表
     * @return 角色列表
     */
    @Select("select * from role")
    List<Role> selectRoleList();

    /**
     * 添加角色
     * @param roleName 角色名称
     * @param createUsername 创建者
     * @return 布尔值
     */
    @Insert("insert into role(rolename,create_by) values(#{roleName},#{createUsername})")
    boolean insertRole(@Param("roleName") String roleName,@Param("createUsername") String createUsername);

    /**
     * 根据角色id删除角色
     * @param roleId 角色id
     * @return 布尔值
     */
    @Delete("delete from role where id=#{roleId}")
    boolean deleteRoleByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据角色id修改角色名称
     * @param roleId 角色id
     * @param newName 新名称
     * @param updateUsername 修改者用户名
     * @return 布尔值
     */
    @Update("update role set rolename=#{newName}, update_by=#{updateUsername}, update_time=CURRENT_TIMESTAMP where id=#{roleId}")
    boolean updateRoleNameByRoleId(@Param("roleId") Long roleId, @Param("newName") String newName, @Param("updateUsername") String updateUsername);

    /**
     * 根据角色名查询角色id
     * @param roleName 角色名称
     * @return 角色id
     */
    @Select("select id from role where rolename=#{roleName}")
    Long selectRoleIdByRoleName(@Param("roleName") String roleName);

    /**
     * 根据角色id查询角色的父级权限列表
     * @param roleId 角色id
     * @return 父级权限列表
     */
    @Select("select id,pid,count_sub,name,uri from role_auth LEFT JOIN auth ON role_auth.auth_id=auth.id where role_id=#{roleId} and pid=0")
    List<Auth> selectParentAuthListByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据角色id查询角色的权限列表
     * @param roleId 角色id
     * @return 权限列表
     */
    @Select("select id,pid,count_sub,name,uri from role_auth LEFT JOIN auth ON role_auth.auth_id=auth.id where role_id=#{roleId}")
    List<Auth> selectAuthListByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据角色id删除角色所有权限
     * @param roleId 角色id
     * @return
     */
    @Delete("delete from role_auth where role_id=#{roleId}")
    boolean deleteRoleAllAuthByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据角色id和权限id删除角色的该权限
     * @param roleId 角色id
     * @param authId 权限id
     * @return
     */
    @Delete("delete from role_auth where role_id=#{roleId} and auth_id=#{authId}")
    boolean deleteRoleAnAuthByRoleIdAndAuthId(@Param("roleId") Integer roleId,@Param("authId") Long authId);

    /**
     * 批量插入角色权限
     * @param roleId 角色id
     * @param authIds 权限id列表
     * @return 布尔值
     */
    @Insert("<script>" +
            "insert into role_auth(role_id,auth_id) values " +
            "<foreach collection='authIds' item='authId' separator=',' >" +
            "   (#{roleId},#{authId})" +
            "</foreach>" +
            "" +
            "</script>")
    boolean insertAuthsToRole(@Param("roleId") Long roleId,@Param("authIds") List<Long> authIds);

    /**
     * 根据权限组 批量删除角色权限
     * @param roleId 角色id
     * @param authIds 权限id列表
     * @return 布尔值
     */
    @Delete("<script>" +
            "delete from role_auth where role_id=#{roleIds}" +
            " and auth_id in " +
            "<foreach collection='authIds' item='authId' open='(' separator=',' close=')'>" +
            "   #{authId}" +
            "</foreach>" +
            "</script>")
    boolean deleteAuthsGroupOfRole(@Param("roleIds") Long roleId, @Param("authIds") List<Long> authIds);

    /**
     * 根据角色id集合查询权限列表
     * @param roleIds
     * @return
     */
    @Select("<script>" +
            "SELECT DISTINCT auth.* FROM role_auth LEFT JOIN auth ON role_auth.auth_id=auth.id WHERE role_auth.role_id IN " +
            "<foreach collection='roleIds' item='roleId' open='(' separator=',' close=')'>" +
            "   #{roleId}" +
            "</foreach>" +
            "</script>")
    List<Auth> listAuthsByRoleIds(@Param("roleIds") List<Long> roleIds);

    /**
     * 禁用角色
     * @param roleId 角色id
     * @return 布尔值
     */
    @Update("update role set is_deleted=1 where id=#{roleId}")
    boolean forbiddenRole(@Param("roleId") Long roleId);

    /**
     * 启用角色
     * @param roleId 角色id
     * @return 布尔值
     */
    @Update("update role set is_deleted=0 where id=#{roleId}")
    boolean unForbiddenRole(@Param("roleId") Long roleId);

    /**
     * 根据角色id和权限id查询该权限是否在角色权限表中
     * @param roleId 角色id
     * @param authId 权限id
     * @return 权限id
     */
    @Select("select auth_id from role_auth where role_id=#{roleId} and auth_id=#{authId}")
    Long isAuthIdInRoleAuth(@Param("roleId") Long roleId,@Param("authId") Long authId);
}
