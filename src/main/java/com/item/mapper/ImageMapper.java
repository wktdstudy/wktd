package com.item.mapper;

import com.item.pojo.Image;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
* @author likaiping
* @description 针对表【image】的数据库操作Mapper
* @createDate 2024-06-09 15:18:50
* @Entity com.item.pojo.Image
*/
public interface ImageMapper extends BaseMapper<Image> {

    /**
     * 查询所有图片
     * @return 图片列表
     */
    @Select("select * from image")
    List<Image> selectAllImage();

    /**
     * 根据图片名称查询图片id
     * @param name 图片名称
     * @return 图片id
     */
    @Select("select id from image where name=#{name}")
    Long selectIdByName(@Param("name") String name);

    /**
     * 根据图片类型查询图片列表 取未逻辑删除的图片
     * @param type 图片类型
     * @return 图片列表
     */
    @Select("select id,name,type,img_url from image where type=#{type} and is_deleted=0")
    List<Image> selectImageListByType(@Param("type") String type);

    /**
     * 逻辑删除图片
     * @param imageId 图片id
     * @return 删除成功与否
     */
    @Update("update image set is_deleted=1 where id=#{imageId}")
    boolean forbiddenImage(@Param("imageId") Long imageId);

    /**
     * 逻辑恢复图片
     * @param imageId 图片id
     * @return 恢复成功与否
     */
    @Update("update image set is_deleted=0 where id=#{imageId}")
    boolean unForbiddenImage(@Param("imageId") Long imageId);

    /**
     * 插入图片
     * @param name 图片名称
     * @param imgUrl 图片路径
     * @return 插入成功与否
     */
    @Insert("insert into image(name,img_url) values(#{name},#{imgUrl})")
    boolean insertImage(@Param("name") String name,@Param("imgUrl") String imgUrl);

    /**
     * 更新图片
     * @param image 图片
     * @return 更新结果
     */
    @Update("<script>" +
            "update image" +
            "<set>" +
            "<if test='name!=null and name!=\"\"'> name=#{name}, </if>" +
            "<if test='imgUrl!=null and imgUrl!=\"\"'> img_url=#{imgUrl}, </if>" +
            "<if test='type!=null and type!=\"\"'> type=#{type} </if>" +
            "</set>" +
            "where id=#{id}" +
            "</script>")
    boolean updateImage(Image image);
}




