package com.item.mapper;

import com.item.pojo.Answer;
import com.item.pojo.Question;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 针对表【question】的数据库操作Mapper
 * @author XXDLD
 * @description 提供对问题表的数据库操作接口
 * @createDate 2024-05-17 14:58:35
 * @Entity com.item.pojo.Question
 */
public interface QuestionMapper extends BaseMapper<Question> {
    /**
     * 查询内容被点击次数大于等于2的问题
     * @return 返回问题的查询结果，这里返回的是字符串形式的结果
     */
    @Select("SELECT * from question WHERE hits >=2")
    List<Question> getQuestionByHits();

    /**
     * 根据问题ID获取所有相关答案的内容。
     *
     * <p>此方法执行SQL查询，从answer表中选择所有与给定问题ID关联的答案内容。
     * 使用@Param注解将方法参数与SQL语句中的占位符进行映射。
     *
     * @param questionId 问题的ID，用于过滤答案。
     * @return 返回一个包含所选问题所有答案内容的String类型List。
     */
    @Select("SELECT id,answer_content from answer WHERE question_id=#{questionId}")
    List<Answer> getAnswerByQuestionId(@Param("questionId") Integer questionId);

    /**
     * 更新问题的点击次数。
     * 通过SQL语句动态更新question表中指定id的问题的hits字段，实现点击计数功能。
     *
     * @param questionId 问题的唯一标识符，用于指定需要更新点击次数的问题。
     *                   此参数作为SQL语句中的条件，确保只对特定问题的点击次数进行增加操作。
     * @return 由于方法返回类型为void，因此此注释说明该方法不返回任何值。
     *         方法的执行结果是更新数据库中问题的点击次数。
     */
    @Update("UPDATE question SET hits=hits+1 WHERE id=#{questionId}")
    void updateQuestionHitsById(@Param("questionId") Integer questionId);

    /**
     * 根据问题内容模糊搜索问题。
     *
     * <p>此方法执行SQL查询，从question表中选择所有包含指定内容的问题。
     * SQL语句使用LIKE操作符和通配符来实现模糊匹配。
     * 使用@Param注解将方法参数与SQL语句中的占位符进行映射。
     *
     * @param content 要搜索的问题内容关键字。
     * @return 返回一个包含所有匹配内容问题的Question对象的List。
     */
    @Select("SELECT id,question_content from question WHERE question_content like '%' #{content} '%'  ")
    List<Question> getQuestionByContent(@Param("content") String content);

}
