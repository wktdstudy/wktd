package com.item.mapper;

import com.item.pojo.Auth;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
* @author likaiping
* @description 针对表【auth(系统菜单)】的数据库操作Mapper
* @createDate 2024-06-04 17:32:03
* @Entity com.item.pojo.Auth
*/
public interface AuthMapper extends BaseMapper<Auth> {

    /**
     * 查询所有权限id
     * @return 权限id列表
     */
    @Select("select id from auth")
    List<Long> selectAllAuthId();

    /**
     * 查询父级权限列表
     * @return 父级权限列表
     */
    @Select("select id,pid,is_deleted,count_sub,name,uri from auth where pid=0")
    List<Auth> selectParentAuthList();

    /**
     * 根据权限id查询父级权限id
     * @param authId 权限id
     * @return pid
     */
    @Select("select pid from auth where id=#{authId}")
    Long selectPidByAuthId(@Param("authId") Long authId);

    /**
     * 根据父级权限id查询子级权限列表
     * @param pid 父级权限id
     * @return 子级权限列表
     */
    @Select("select id,pid,count_sub,name,uri from auth where pid=#{pid}")
    List<Auth> selectChildAuthsListByPid(@Param("pid") Long pid);

    /**
     * 根据父级权限名称查询父级权限id
     * @param pidName 父级权限名
     * @return 父级权限id
     */
    @Select("select id from auth where name=#{pidName}")
    Long selectIdByPidName(@Param("pidName") String pidName);

    /**
     * 根据父级权限id查询子级权限id
     * @param pid 父级权限id
     * @return 子权限列表
     */
    @Select("select id from auth where pid=#{pid}")
    List<Long> listAuthIdsByPid(@Param("pid") Long pid);

    /**
     * 修改权限信息  <set></set>标签会自动处理最后一个逗号问题
     * @param id 权限id
     * @param name 权限名称
     * @param uri 权限uri
     * @return 修改结果
     */
    @Update("<script>" +
            "update auth set " +
            "<if test=' name!=null and name!=\"\" '> name=#{name}, </if>" +
            "<if test=' uri!=null and uri!=\"\" '> uri=#{uri}, </if>" +
            "update_time=CURRENT_TIMESTAMP " +
            "where id = #{id}" +
            "</script>")
    boolean updateAuthInfo(@Param("id") Long id,@Param("name") String name,@Param("uri") String uri);

    /**
     * 禁用权限
     * @param authId 权限id
     * @return 禁用结果
     */
    @Update("update auth set is_deleted=1, update_time=CURRENT_TIMESTAMP where id=#{authId}")
    boolean forbiddenAuth(@Param("authId") Long authId);

    /**
     * 禁用子级权限
     * @param pid 父级权限id
     * @param authIds 子级权限列表
     * @return 禁用结果
     */
    @Update("<script>" +
            "update auth set is_deleted=1, update_time=CURRENT_TIMESTAMP where pid=#{pid} and id in " +
            "<foreach collection='authIds' item='authId' open='(' separator=',' close=')'>" +
            "   #{authId}" +
            "</foreach>" +
            "</script>")
    boolean forbiddenChildAuths(@Param("pid") Long pid, @Param("authIds") List<Long> authIds);

    /**
     * 解禁权限
     * @param authId 权限id
     * @return 解禁结果
     */
    @Update("update auth set is_deleted=0, update_time=CURRENT_TIMESTAMP where id=#{authId}")
    boolean unForbiddenAuth(@Param("authId") Long authId);

    /**
     * 解禁子级权限
     * @param pid 父级权限id
     * @param authIds 子级权限列表
     * @return 解禁结果
     */
    @Update("<script>" +
            "update auth set is_deleted=0, update_time=CURRENT_TIMESTAMP where pid=#{pid} and id in " +
            "<foreach collection='authIds' item='authId' open='(' separator=',' close=')'>" +
            "   #{authId}" +
            "</foreach>" +
            "</script>")
    boolean unForbiddenChildAuths(@Param("pid") Long pid, @Param("authIds") List<Long> authIds);

    /**
     * 插入父级权限
     * @param name 权限名称
     * @param uri 权限uri
     * @return 插入结果
     */
    @Insert("insert into auth(pid,count_sub,name,uri) values(0,0,#{name},#{uri})")
    boolean insertParentAuth(@Param("name") String name,@Param("uri") String uri);

    /**
     * 插入子级权限
     * @param pid 父级权限id
     * @param name 权限名称
     * @param uri 权限uri
     * @return 插入结果
     */
    @Insert("insert into auth(pid,name,uri) values(#{pid},#{name},#{uri})")
    boolean insertChildAuthInParentAuth(@Param("pid") Long pid,@Param("name") String name, @Param("uri") String uri);

    /**
     * 更新父级权限的子级权限数量 自动+1
     * @param pid 父级权限id
     * @return 更新结果
     */
    @Update("update auth set count_sub=count_sub+1 where id=#{pid}")
    boolean updateParentAuthCountSub(@Param("pid") Long pid);

    /**
     * 删除父级权限
     * @param pid 父级权限id
     * @return 删除结果
     */
    @Delete("delete from auth where id=#{pid}")
    boolean deleteParentAuth(@Param("pid") Long pid);

    /**
     * 删除父级权限的所有子级权限
     * @param pid 父级权限id
     * @return 删除结果
     */
    @Delete("delete from auth where pid=#{pid}")
    boolean deleteChildAuthsInParentAuth(@Param("pid") Long pid);

    /**
     * 删除子级权限
     * @param authId 子级权限id
     * @return 删除结果
     */
    @Delete("delete from auth where id=#{authId}")
    boolean deleteAnChildAuth(@Param("authId") Long authId);

    /**
     * 根据父级权限id查询角色id列表
     * @param pidAuthId 父级权限id
     * @return 角色id列表
     */
    @Select("select role_id from role_auth where auth_id=#{pidAuthId}")
    List<Long> selectRoleIdByPidAuthId(@Param("pidAuthId") Long pidAuthId);

    /**
     * 批量给几个角色插入一个权限
     * @param roleIds 角色id列表
     * @param authId 权限id
     * @return 布尔值
     */
    @Insert("<script>" +
            "insert into role_auth(role_id,auth_id) values " +
            "<foreach collection='roleIds' item='roleId' separator=',' >" +
            "   (#{roleId},#{authId})" +
            "</foreach>" +
            "" +
            "</script>")
    boolean insertAuthToRoles(@Param("roleIds") List<Long> roleIds,@Param("authId") Long authId);
}




