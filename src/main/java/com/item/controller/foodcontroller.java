package com.item.controller;

import com.item.pojo.*;
import com.item.service.foodservice;
import com.item.service.item.GetridismessageService;
import com.item.service.item.MessagePublisherService;
import com.item.utils.RedisUtil;
import lombok.AllArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@RestController
@RequestMapping("/food")
public class foodcontroller {
    @Autowired
    private foodservice foodservice;
    @Value("${file.upload.path}")
    private String uploadPath;
    @Autowired
    private MessagePublisherService messagePublisherService;
    @Autowired
    public RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private GetridismessageService getridismessageService;
    /**
     * 根据类型获取食物信息
     *
     * @param type 食物类型，用于筛选菜单项
     * @return 返回操作结果，如果成功返回包含菜单项的Result对象，如果失败返回错误信息的Result对象
     */
    @PostMapping("/getfood")
    public Result getfood(String type) {
        // 调用服务层方法，根据类型获取食物菜单
        List<MenuItem> menuItems = foodservice.getfood(type);
        if (menuItems != null) {
            // 如果获取到食物菜单，返回成功结果并附带菜单项列表
            return Result.success(menuItems);

        }
        // 如果未获取到食物菜单，返回错误结果
        return Result.error("查询失败");
    }

    @PostMapping("/orderfood")
    public Result orderfood(BigDecimal price, int id, int ammount) {
        System.out.println(price);
String userstringid= (String) redisTemplate.opsForValue().get("userid");

int userid= Integer.parseInt(userstringid);
        System.out.println(userid);
        try {
            foodservice.insertto0rder(price, id, userid, ammount);
            return Result.success();
        } catch (Exception e) {
            return Result.error(String.valueOf(e));
        }


    }


    @PostMapping("/getorder")
    public Result getorder(){
        String userstringid= (String) redisTemplate.opsForValue().get("userid");

        int userid= Integer.parseInt(userstringid);
try{
   List<OrderDetailsResponse>orderDetailsResponses= foodservice.getorder(userid);
    return Result.success(orderDetailsResponses);
} catch (Exception e) {
    return Result.error(String.valueOf(e));
}

    }
   @PostMapping("/getamountprice")
public  Result getamountprice(){
       String userstringid= (String) redisTemplate.opsForValue().get("userid");
       int userid= Integer.parseInt(userstringid);
       try{
          BigDecimal totalamount=foodservice.getorderamount(userid);
           return Result.success(totalamount);
       } catch (Exception e) {
           return Result.error(String.valueOf(e));
       }


   }
    @PostMapping("/getcouponsamounts")
    public  Result getcouponsamounts(int couponId){
        String userstringid= (String) redisTemplate.opsForValue().get("userid");
        int userid= Integer.parseInt(userstringid);
        redisTemplate.opsForValue().set("couponId",couponId);
        try{
            BigDecimal totalamount=foodservice.changeamountbycoupon(userid,couponId);
            return Result.success(totalamount);
        } catch (Exception e) {
            return Result.error(String.valueOf(e));
        }


    }
    @PostMapping("/changeorder")
    public Result changeorde(BigDecimal price, int id, int ammount) {
        System.out.println(price);
        String userstringid= (String) redisTemplate.opsForValue().get("userid");

        int userid= Integer.parseInt(userstringid);
        System.out.println(userid);
        try {
            foodservice.insertto0rder(price, id, userid, ammount);
            return Result.success();
        } catch (Exception e) {
            return Result.error(String.valueOf(e));
        }


    }

    @PostMapping("/deletorderdetail")
    public  Result deletorderdetail(int itemId){
        String userstringid= (String) redisTemplate.opsForValue().get("userid");
        int userid= Integer.parseInt(userstringid);
        try{
            foodservice.deletOrderDetailByitemId(itemId,userid);
            return Result.success();
        } catch (Exception e) {
           return Result.error("删除失败");
        }
    }



@PostMapping("/updateorderstatus")
public Result updateorderstatus()
{
    String userstringid= (String) redisTemplate.opsForValue().get("userid");
    System.out.println("userid"+userstringid);
    int userid= Integer.parseInt(userstringid);
    if(foodservice.ConfirmOrder(userid))
    {
        return Result.success();
    }

   else {
        return Result.error("删除失败");
    }



}

@PostMapping("/modifysales")
    public  Result Modifysales(int itemId){
    String userstringid= (String) redisTemplate.opsForValue().get("userid");
    int userid= Integer.parseInt(userstringid);
        if ( foodservice.Modifysales(itemId,userid)){
            return Result.success();

        }else {

            return Result.error("add sales is false");
        }


    }
@PostMapping("/searchfood")
public Result searchfood(String itemname){
        if(itemname!=null){
            List<MenuItem> menuItems=new ArrayList<>();
           menuItems= foodservice.searchfood(itemname);
            return Result.success( menuItems);
        }
        else {
            return Result.error("模糊查询菜名失败");
        }
}

@PostMapping("/findspecialmenu")
    public Result findspecialmenu(String type){
        if(foodservice.findspecialmenu(type)!=null){
            List<MenuItem> menuItems=new ArrayList<>();
            menuItems=foodservice.findspecialmenu(type);
            return Result.success(menuItems);
        }else {
            return Result.error("特价菜单查询失败");

        }


}

@PostMapping("/spikesorder")
    public Result Spikesorder(int id ,BigDecimal price,int ammount){
    String userstringid= (String) redisTemplate.opsForValue().get("userid");
    int userid= Integer.parseInt(userstringid);
    if(foodservice.Spikesmenu(id,userid,price,ammount))
    {
        return Result.success();
    }else {
        return Result.error("你有账单未支付");

    }
}

@PostMapping("/spikesorderRedisTest")
    public Result SpikeorderRedisTest(int itemid){
    Random random=new Random();
   int userid=random.nextInt();
    if(foodservice.SpikesmenuRedis(itemid,userid))
    {
        return Result.success();
    }else {
        return Result.error("你有账单未支付");

    }
}
@PostMapping("/getmenupack")
    public Result getmenupack() {
        try{
            List<itempack> itempacks=new ArrayList<>();
            itempacks=  foodservice.finditempack();

            return Result.success(itempacks);
        } catch (Exception e) {
            return Result.error(String.valueOf(e));
        }

}
@PostMapping("/getcomment")
public Result getcomment(int itemid){
        try{
           List<ItemComment>itemComments= foodservice.getcomment(itemid);
            return  Result.success(itemComments);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(String.valueOf(e));
        }


}
@PostMapping("/getdowncomment")
    public Result getdowncomment(int id,int itemid){
     try {
      List<ItemComment>comments=  foodservice.getdowncomment(id,itemid);
      return Result.success(comments);
     } catch (Exception e) {
         e.printStackTrace();
         return Result.error(String.valueOf(e));
     }

}


@PostMapping("/getuserdetail")
    public Result getuserdetail(int userId){
try {

    return Result.success(foodservice.getuserdetail(userId));
} catch (Exception e) {
    e.printStackTrace();
    System.out.println(e);
    return Result.error(String.valueOf(e));
}

}

@PostMapping("/insertcomment")
    public Result insertcomment(String commenttext, double rating, int pid,int itemId){
    String userstringid= (String) redisTemplate.opsForValue().get("userid");
    int userId= Integer.parseInt(userstringid);
try {
    foodservice.insertcomment(commenttext,rating,pid,userId,itemId);
    return Result.success();
} catch (Exception e) {
    e.printStackTrace();
    return Result.error(String.valueOf(e));
}

}
@PostMapping("/findorderdetail")
    public Result findorderdetail(){
    String userstringid= (String) redisTemplate.opsForValue().get("userid");
    int userid= Integer.parseInt(userstringid);
    try {
        List<OrderDetailsResponse> orderDetails=foodservice.findordetailbyuserIdandnumber(userid);
        return Result.success(orderDetails);
    } catch (Exception e) {
        e.printStackTrace();
        return Result.error(String.valueOf(e));
    }
}

@PostMapping("/getorderdetailamount")
    public Result getorderdetailamount(){
    String userstringid= (String) redisTemplate.opsForValue().get("userid");
    int userid= Integer.parseInt(userstringid);
    try {
        BigDecimal totalamount=foodservice.findtotalamountByuseridandnumber(userid);
        return Result.success(totalamount);
    } catch (Exception e) {
        e.printStackTrace();
        return Result.error(String.valueOf(e));
    }
}
@PostMapping("/findorderidByuseridandnumber")
    public Result findorderidByuseridandnumber(){
    String userstringid= (String) redisTemplate.opsForValue().get("userid");
    int userid= Integer.parseInt(userstringid);
    try {
       int orderId=foodservice.findorderidByuseridandnumber(userid);
        return Result.success(orderId);
    } catch (Exception e) {
        e.printStackTrace();
        return Result.error(String.valueOf(e));
    }
}
@PostMapping("/findorderstatusByuseridandnumber")
    public Result findorderstatusByuseridandnumber(){
        String userstringid= (String) redisTemplate.opsForValue().get("userid");
        int userid= Integer.parseInt(userstringid);
        try {
            String orderstatus=foodservice.findorderstatusByuseridandnumber(userid);
            return Result.success(orderstatus);

        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(String.valueOf(e));
        }




}

@PostMapping("/findorderByuserid")
    public Result findorderByuserid(){
        String userstringid= (String) redisTemplate.opsForValue().get("userid");
        int userid= Integer.parseInt(userstringid);
        try {
List<Order> orders=foodservice.findorderByuserid(userid);
            return Result.success(orders);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(String.valueOf(e));

        }


}

@PostMapping("/finditemlistByOrderId")
     public Result finditemlistByOrderId(int orderId){
        try {
            List<MenuItem> menuItems=foodservice.finditemlistByOrderId(orderId);
            return Result.success(menuItems);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(String.valueOf(e));
        }
    }

    @PostMapping("/findorderdetailbyorderId")
    public Result findorderdetailbyorderId(int orderId){

        try {
            List<OrderDetailsResponse> orderDetails=foodservice.findordetailbyorderId(orderId);
            return Result.success(orderDetails);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(String.valueOf(e));
        }
    }

    @PostMapping("/findorderstatusByorderId")
    public Result findorderstatusByorderId(int orderId){

        try {
            String orderstatus=foodservice.findorderstatusByorderId(orderId);
            return Result.success(orderstatus);

        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(String.valueOf(e));
        }




    }
    @PostMapping("/getorderdetailamountbyorderId")
    public Result getorderdetailamount(int orderId){
        try {
            BigDecimal totalamount=foodservice.findtotalamountByorderId(orderId);
            return Result.success(totalamount);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(String.valueOf(e));
        }
    }
@PostMapping("/getcouponByuserId")
    public Result getcouponByuserId(){
        String userstringid= (String) redisTemplate.opsForValue().get("userid");
        int userid= Integer.parseInt(userstringid);
        try {
            List<Coupon> coupons = foodservice.getcouponByuserId(userid);
            return Result.success(coupons);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(String.valueOf(e));
        }

    }
    @PostMapping("/getallcoupons")
    public Result getallcoupons(){
        try {
            List<Coupon> coupons=foodservice.getallcoupons();
            return  Result.success(coupons);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }

    }
    @PostMapping("/getuserallrole")
    public Result getuserallrole(){
        try {
            List<Role> roles=foodservice.getallrole();
            return Result.success(roles);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }

    @PostMapping("/getcoupons")
    public Result getcoupons(int couponId){
        try {
            if( foodservice.getcoupons(couponId)){
                return Result.success();
            }else {
                return Result.error("优惠券已失效,或已经领取过");
            }


        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }

    }
    @PostMapping("/getrecommenditem")
    public Result getrecommenditem(){

        try {
            String userstringid= (String) redisTemplate.opsForValue().get("userid");
            int userid= Integer.parseInt(userstringid);
            List<Integer> userList=foodservice.getcommenditmeId(userid);
            return Result.success( foodservice.getmenurecommendbyuserId(userList));
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(String.valueOf(e));
        }
    }
    @PostMapping("/insertorderdetailBypackItem")
    public Result insertorderdetailBypackItem(BigDecimal totalamount,int packId)
    {
        try{
            String userstringid= (String) redisTemplate.opsForValue().get("userid");
            int userid= Integer.parseInt(userstringid);
            if(foodservice.insertorderdetailBypackItem(userid,totalamount,packId)){
                return Result.success();
            }else {
                return Result.error("添加失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }


    }
    @PostMapping("/getmenuitemBypackId")
    public Result getmenuitemBypackId(int packId){
        try {
            List<MenuItem> menuItems=foodservice.getListmenuBypackId(packId);
            return Result.success(menuItems);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/getitemListByrecomenditemId")
    public Result getitemListByrecomenditemId(int itemId){
        try {
            List<Integer> itemIdList=foodservice.getitemIdByrecomend(itemId);
            List<MenuItem> menuItems=foodservice.menurecommendByitemId(itemIdList);
            return Result.success(menuItems);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/getmenListBysalas")
    public Result getmenListBysalas(){
        try {
            List<MenuItem> menuItems=foodservice.menuListBysales();
            return Result.success(menuItems);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/getmenuListByavagerating")
    public Result getmenuListByavagerating(){
        try {
            List<Integer> menuidItems=foodservice.menuidListByAveragerating();
            List<MenuItem> menuItems=foodservice.menuListByavagerItemList(menuidItems);
            return Result.success(menuItems);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/deleleorderandorderDetailByorderId")
    public Result deleleorderandorderDetailByorderId(int orderId){
        try {
            if(foodservice.deletorderanorderdetail(orderId)){
                return Result.success();
            }else {
                return Result.error("删除失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }




    //从这里开始就是后台管理的service层
    @PostMapping("/getalluser")
    public Result getalluser(){
        try {
            List<User> users=foodservice.getAlluserlist();
            return Result.success(users);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/updatepasswordByusername")
    public Result updatepasswordByusername(String username,String password){
        try {
            if(foodservice.updatepassword(username,password)){
                return Result.success();
            }else {
                return Result.error("修改失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/updateuser")
    public Result updateuser(int id,String username, String email,  String phone, int age,String imageurl){
        try {
            if(foodservice.updateuser(id,username,email,phone,imageurl,age)){
                return Result.success();
            }else {
                return Result.error("修改失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/updateuserimageurl")
    public Result handleFileUpload( MultipartFile file) {
        if (file.isEmpty()) {
            return Result.error("上传失败");
        }

        try {
            // 生成一个唯一的文件名
            String uniqueFileName = UUID.randomUUID().toString() + "_" + file.getOriginalFilename();

            // 创建文件路径
            Path path = Paths.get(uploadPath, uniqueFileName);

            // 将MultipartFile保存到指定目录
            Files.createDirectories(path.getParent());
            file.transferTo(path.toFile());
          path=Paths.get("../src/assets/", uniqueFileName);
            // 返回文件路径
            return Result.success(path.toString());
        } catch (Exception e) {
            return Result.error("Failed to upload the file.");
        }
    }

    @PostMapping("/getallRolemange")
    Result getallrole(){
        try {
            List<Role> roles=foodservice.getallRolemange();
            return Result.success(roles);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/getroleByusername")
    Result getroleByusername(String username){
        try {
            List<Role> roles=foodservice.getallroleByusername(username);
            return Result.success(roles);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }

    @PostMapping("/insertuserRoleByuseridandroleid")
    public Result insertUserRoleByUserIdAndRoleId(@RequestBody UserRoleRequest request) {
        try {
            List<Integer> roleIds = request.getRoleid();
            int userId = request.getUserid();

            if (foodservice.insertuserRoleByuseridandroleid(userId, roleIds)) {
                return Result.success();
            } else {
                return Result.error("修改失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }

    @PostMapping("/updateuserStatus")
    public Result updateuserStatus(int userid){
        try {
            if(foodservice.updateUserstatus(userid)){
                return Result.success();
            }else {
                return Result.error("修改失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }

    @PostMapping("/getmenuitemList")
    public Result getmenuitemList(){
        try {
            List<MenuItem> menuItems=foodservice.getallmenuList();
            return Result.success(menuItems);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    };
    @PostMapping("/insertmenuBybakground")
    public Result insertmenuBybakground(@RequestBody MenuItem menuItem){
        if(menuItem!=null && menuItem.getItemname()!=null && menuItem.getPrice()!=null && menuItem.getDescription()!=null){
            try {
                if(foodservice.insertmenuBybakground(menuItem)){
                    return Result.success();
                }else {
                    return Result.error("修改失败");
                }
            } catch (Exception e) {
                e.printStackTrace();
                return Result.error(e.getMessage());
            }

        }else {
            return Result.error("内容不能为空");
        }

    }

    @PostMapping("/getitemByitemId")
    public Result getitemByitemId(int itemId){
        try {
            MenuItem menuItem=foodservice.getmenuByid(itemId);
            return Result.success(menuItem);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }

    @PostMapping("/updateitemByitem")
    public Result updateitemByitem(@RequestBody MenuItem menuItem){
        try {
            if(foodservice.updateitemByitem(menuItem)){
                return Result.success();
            }else {
                return Result.error("修改失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }

    @PostMapping("/updateitemspecialPriceandtypeandquantityByitemId")
    public Result updateitemspecialPriceandtypeandquantityByitemId(@RequestBody MenuItem menuItem){
        try {
            if(foodservice.updateitemspecialPriceandtypeandquantityByitemId(menuItem)){
                return Result.success();
            }else {
                return Result.error("修改失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }


    @PostMapping("/updateitemstatusByitemId")
    public Result updateitemstatusByitemId(int itemId){

        try {
            if(foodservice.updateitemstatusByitemId(itemId)){
                return Result.success();
            }else {
                return Result.error("修改失败");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/updateitempriceByitemid")
    public Result updateitempriceByitemid(int itemId,BigDecimal price){
        try {
            if(foodservice.updateitempriceByitemid(itemId,price)){
                return Result.success();
            }else {
                return Result.error("修改失败");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }

    }
@PostMapping("/getorderBybackground")
    public Result getorderBybackground()
    {
        try {
            List<Order> orders=foodservice.getorderBybackground();
            return Result.success(orders);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }


    }
    @PostMapping("/updateorderstatusByorderId")
    public Result updateorderstatusByorderId(int orderId,String status){
        try {
            if(foodservice.updateorderstatusByorderId(orderId,status)){
                return Result.success();
            }else {
                return Result.error("修改失败");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/sentmessageByredis")
    public Result sentmessageByredis(String message,String userId,int orderId){
        redisTemplate.opsForValue().set("userid2",userId);
        try {
            message=message+"订单单号为"+orderId+"的菜品"+"的菜品,请稍作等待";
            messagePublisherService.publishToCommon(message);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/getmessageByredis")
    public Result getmessageByredis(){
        try {
           String message=getridismessageService.getmessage();
            return Result.success(message);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/sentitemmessageByredis")
    public Result sentitemmessageByredis(String message,String type,BigDecimal Discount){
        System.out.println(Discount);

        try {

if(type.equals("全部")){

   if(foodservice.diccountitemorice(Discount) ){
       String discount=Discount.toString();
       redisTemplate.opsForValue().set("type",type);
       redisTemplate.opsForValue().set("Discount",discount);
       message="网咖周末日，"+message+"凭借网咖优惠卷可减免更多,欢迎给大会员积极下单";
       messagePublisherService.publishToItem(message);
       return Result.success();
    }
   else return Result.error("修改失败");
}else {

    if(foodservice.diccountitemoriceBytype(type,Discount) ){
        String discount=Discount.toString();
        redisTemplate.opsForValue().set("type",type);
        redisTemplate.opsForValue().set("Discount",discount);
        messagePublisherService.publishToItem(message);
        return Result.success();
    }
    else return Result.error("修改失败");
}
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping( "/getItemdiscountpricemessage")
    public Result getItemdiscountpricemessage(){
        try {
            String message=getridismessageService.getitemmessage();
            return Result.success(message);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }

    }


    @PostMapping( "/recoveritemorice")
    public Result recoveritemorice(){
        try {
            if(foodservice.recoveritemorice()){
                return Result.success();
            }else {
                return Result.error("修改失败");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/getallcouponsBybackground")
    public Result getallcouponsBybackground(){
        try {
            List<Coupon> coupons=foodservice.getallcouponsBybackground();
            return Result.success(coupons);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }

    }
    @PostMapping("/insertcoupon")
    public Result insertcoupon(@RequestBody Coupon coupon){
        try {
            if(foodservice.insertcoupon(coupon)){
                return Result.success();
            }else {
                return Result.error("修改失败");
            }
        }catch (Exception e){
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }

    @PostMapping("/getcouponByid")
    public Result getcouponByid(int couponId){
        try {
            Coupon coupon=foodservice.getcouponByid(couponId);
            return Result.success(coupon);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/updatecoupon")
    public Result updatecoupon(@RequestBody Coupon coupon){
        try {
            if(foodservice.updatecoupon(coupon)){
                return Result.success();
            }else {
                return Result.error("修改失败");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/setcouponTouser")
    public Result sentcouponTOuser(int userId,int couponId){
        try {
            redisTemplate.opsForValue().set("userId3",userId);
           messagePublisherService.publishTocoupon("您收到有一张优惠卷，请及时使用");
           foodservice.setcouponTouser(couponId,userId);
           return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }

    @PostMapping("/getcouponmessage")
    public Result getcouponmessage(){
        try {
            String message=getridismessageService.getcouponmessage();
            return Result.success(message);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }

    @PostMapping("/finduserBYcouponId")
    public Result findUserBycouponId(int couponId){
        try {
            List<User> users=foodservice.finduserBYcouponId(couponId);
            return Result.success(users);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }

    @PostMapping("/deleteUsercouponByuserIdandcouponId")
    public Result deleteUsercouponByuserIdandcouponId( int userId,int couponId){
        try {
            foodservice.deleteUsercouponByuserIdandcouponId(couponId,userId);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/delercoupon")
    public Result deletcoupon(int id){
        try {
            if(foodservice.deletcouponByid(id)){
                return Result.success();
            }else {
                return Result.error("删除失败");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/getallrcomment")
    public Result getallrcomment(){
        try {
            List<ItemComment> comments=foodservice.getallcomment();
            return Result.success(comments);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/insertcommentByidanditemid")
    public Result insertcommentByidanditemid(String comment,int pid,int itemId){
        try {
            foodservice.insertcommentByidanditemid(comment,pid,itemId);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/updatecommentweightByid")
    public Result updatecommentweightByid(int id,int itemId){
        try {
            foodservice.updatecommentweightByid(id,itemId);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/updatecommentisdeleteByid")
    public Result updatecommentisdeleteByid(int id){
        try {
            foodservice.updatecommentisdeleteByid(id);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping( "/getallitempackBybackground")
    public Result getallitempackBybackground(){
        try {
            List<itempack> itempacks=foodservice.getallitempackBybackground();
            return Result.success(itempacks);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/insertitempackBybakground")
    public Result insertitempackBybakground(String description,String imageUrl){
        try {
            foodservice.insertitempackBybakground(description,imageUrl);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/updateitempackdescriptionByid")
    public Result updateitempackdescriptionByid(int id,String description,String imageUrl){
        try {
            foodservice.updateitempackdescriptionByid(id,description,imageUrl);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/getimenuListByitemPackId")
    public Result getimenuListByitemPackId(int packId){
        try {
            List<MenuItem> menuItems=foodservice.getListmenuBypackId(packId);
            return Result.success(menuItems);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/getallpriceBypackId")
    public Result getallpriceBypackId(int packId){
        try {
            BigDecimal price=foodservice.getallpriceBypackId(packId);
            return Result.success(price);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/deletepackitemItemByitemIdandpackId")
    public Result deletepackitemItemByitemIdandpackId(int itemId,int packId){
        try {
            foodservice.deletepackitemItemByitemIdandpackId(itemId,packId);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/insertpackitemItemByitemIdandpackId")
    public Result insertpackitemItemByitemIdandpackId(int itemId,int packId){
        try {
            foodservice.insertpackitemItemByitemIdandpackId(itemId,packId);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/updateitempackallpriceByid")
    public Result updateitempackallpriceByid(int id,BigDecimal allprice){
        try {
            foodservice.updateitempackallpriceByid(id,allprice);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/updeteitempackallpriceByaccountandid")
    public Result updeteitempackallpriceByaccountandid(int id,BigDecimal account){
        try {
            foodservice.updeteitempackallpriceByaccountandid(id,account);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/updateallitempackallpriceByaccountandid")
    public Result updateallitempackallpriceByaccountandid(BigDecimal account){
        try {
            foodservice.updateallitempackallpriceByaccountandid(account);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/updateitempackisdeleteByid")
    public Result updateitempackisdeleteByid(int id){
        try {
            foodservice.updateitempackisdeleteByid(id);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/gettaldayorderaomunt")
    public Result gettaldayorderaomunt(){
        try {
            BigDecimal amount=foodservice.gettaldayorderaomunt();
            return Result.success(amount);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }

    }
    @PostMapping("/getbetterOrderpercent")
    public Result getbetterOrderpercent(){
        try {
            BigDecimal percent=foodservice.getbetterOrderpercent();
            return Result.success(percent);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/getyesterdayorderaomunt")
    public Result getyesterdayorderaomunt(){
        try {
            BigDecimal yestodayamount=foodservice.getyesterdayorderaomunt();
            return Result.success( yestodayamount);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/menulistBysalsasLimteten")
    public Result menulistBysalsasLimteten(){
        try {
            List<MenuItem> menuItems=foodservice.menulistBysalsasLimteten();
            return Result.success(menuItems);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/itemcommentListByuserId")
    public Result itemcommentListByuserId(){
        try {
            String userIdstr= (String) redisTemplate.opsForValue().get("userid");
            int userId=Integer.parseInt(userIdstr);
            List<ItemComment> itemComments=foodservice.itemcommentListByuserId(userId);
            return Result.success(itemComments);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
@PostMapping("/getitemcommentByuserIdandpid")
    public Result getitemcommentByuserIdandpid(int pid){
        try {
            List<ItemComment >itemComment=foodservice.getitemcommentByuserIdandpid(pid);
            return Result.success(itemComment);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }

    @PostMapping("/getbestcoupon")
    public Result getbestcoupon(BigDecimal price){
        try {
            String userIdstr= (String) redisTemplate.opsForValue().get("userid");
            int userId=Integer.parseInt(userIdstr);
            Coupon coupon=foodservice.getbestcoupon(price,userId);
            return Result.success(coupon);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
    @PostMapping("/deleteorderandorderDetailByuserId")
    public Result deleteorderandorderDetailByuserId(){
        try {
            String userIdstr= (String) redisTemplate.opsForValue().get("userid");
            int userId=Integer.parseInt(userIdstr);
           foodservice.deleteorderandorderDetailByuserId(userId);
            return Result.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            return Result.error(e.getMessage());
        }
    }
}