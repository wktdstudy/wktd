package com.item.controller;
import com.item.pojo.Auth;
import com.item.pojo.Result;
import com.item.pojo.Role;
import com.item.service.roleservice;
import com.item.utils.GetUsernameUtil;
import lombok.AllArgsConstructor;
import org.apache.ibatis.annotations.Delete;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色和权限管理
 * @author likaiping
 * @createTime 2024/6/4 17:28
 */
@RestController
@AllArgsConstructor
@RequestMapping("/roleManager")
public class RoleManagerController {
    private roleservice roleservice;

    /**
     * 获取角色列表
     * @return 角色列表
     */
    @GetMapping("/getRoleList")
    public Result<List<Role>> findRoleList() {
        List<Role> roleList = roleservice.findAllRoleList();
        return Result.success(roleList);
    }

    /**
     * 添加角色
     * @param roleName 角色名
     * @param token token用来获取创建者名称
     * @return 响应结果信息
     */
    @PutMapping("/addRole")
    public Result addRole(@RequestParam String roleName, @RequestHeader("Authorization") String token) {
        String createUsername = GetUsernameUtil.getUsername(token);
        if (roleservice.addRole(roleName, createUsername)){
            return Result.success("添加角色成功！");
        }else {
            return Result.error("添加角色失败！");
        }
    }

    /**
     * 删除角色
     * @param roleName 需要删除的角色名称
     * @return 响应结果信息
     */
    @DeleteMapping("/removeRole")
    public Result removeRole(@RequestParam String roleName) {
        if (roleservice.removeRoleByRoleName(roleName)) {
            return Result.success("删除角色成功！");
        }else {
            return Result.error("删除角色失败！");
        }
    }

    /**
     * 修改角色
     * @param roleName 角色名
     * @param token token用来获取创建者名称
     * @return 响应结果信息
     */
    @PostMapping("/editRole")
    public Result editRole(@RequestParam String roleName,@RequestParam String newName, @RequestHeader("Authorization") String token) {
        String createUsername = GetUsernameUtil.getUsername(token);
        if (roleservice.editRoleName(roleName, newName, createUsername)) {
            return Result.success("修改角色成功！");
        }else {
            return Result.error("修改角色失败！");
        }
    }

    /**
     * 获取角色父级（一级）权限列表
     * @param roleName 角色名称
     * @return 角色父级权限列表
     */
    @GetMapping("/getRoleParentAuthList")
    public Result<List<Auth>> getRoleParentAuthList(@RequestParam String roleName) {
        List<Auth> authList = roleservice.getRoleParentAuthList(roleName);
        if (!authList.isEmpty()) {
            return Result.success(authList);
        }
        return Result.error("该角色暂无任何父级权限");
    }

    /**
     * 获取角色权限列表
     * @param roleName 角色名称
     * @return 角色权限列表
     */
    @GetMapping("/getRoleAuthList")
    public Result<List<Auth>> getRoleAuthList(@RequestParam String roleName) {
        List<Auth> authList = roleservice.getRoleAuthList(roleName);
        if (!authList.isEmpty()) {
            return Result.success(authList);
        }
        return Result.error("该角色暂无任何权限");
    }

    /**
     * 添加权限组
     * @param roleName 角色名称
     * @param authPidName 父级权限名称
     * @return 响应结果信息
     */
    @PostMapping("/addAuthsToRole")
    public Result addAuthsGroupToRole(@RequestParam String roleName, @RequestParam String authPidName) {
        if (roleservice.addAuthsToRole(roleName,authPidName)) {
            return Result.success("添加权限组成功！");
        }else {
            return Result.error("添加权限组失败！");
        }
    }

    /**
     * 删除权限组
     * @param roleName 角色名称
     * @param authPidName 父级权限名称
     * @return 响应结果信息
     */
    @DeleteMapping("/removeAuthsGroupOfRole")
    public Result removeAuthsGroupOfRole(@RequestParam String roleName, @RequestParam String authPidName) {
        if (roleservice.removeAuthsGroupOfRole(roleName,authPidName)) {
            return Result.success("删除权限组成功！");
        }else {
            return Result.error("删除权限组失败！");
        }
    }

    /**
     * 删除角色所有权限
     * @param roleName 角色名称
     * @return 响应结果信息
     */
    @DeleteMapping("/removeAllAuthsOfRole")
    public Result removeAllAuthsOfRole(@RequestParam String roleName) {
        if (roleservice.removeAllAuthsOfRole(roleName)) {
            return Result.success("删除角色所有权限成功！");
        }else {
            return Result.error("删除角色所有权限失败！");
        }
    }

    /**
     * 为角色添加所有权限
     * @param roleName 角色名称
     * @return 响应结果信息
     */
    @PutMapping("/addAllAuthsToRole")
    public Result addAllAuthsToRole(@RequestParam String roleName) {
        if (roleservice.addAllAuthsToRole(roleName)) {
            return Result.success("为角色添加所有权限成功！");
        }else {
            return Result.error("为角色添加所有权限失败！");
        }
    }

    /**
     * 禁用角色
     * @param roleName 角色名称
     * @return 响应结果信息
     */
    @GetMapping("/forbiddenRole")
    public Result forbiddenRole(@RequestParam String roleName) {
        if (roleservice.forbiddenRole(roleName)) {
            return Result.success("禁用角色成功！");
        }else {
            return Result.error("禁用角色失败！");
        }
    }

    /**
     * 禁用角色
     * @param roleName 角色名称
     * @return 响应结果信息
     */
    @GetMapping("/unForbiddenRole")
    public Result unForbiddenRole(@RequestParam String roleName) {
        if (roleservice.unForbiddenRole(roleName)) {
            return Result.success("解禁角色成功！");
        }else {
            return Result.error("解禁角色失败！");
        }
    }
}
