package com.item.controller;

import com.item.pojo.Image;
import com.item.pojo.Result;
import com.item.service.ImageService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @author likaiping
 * @createTime 2024/6/9 15:30
 */
@RestController
@RequestMapping("/image")
@AllArgsConstructor
public class ImageManagerController {
    private ImageService imageService;

    /**
     * 登录和主页轮播图获取图片 默认自动放行
     * @param type 图片类型
     * @return 图片列表
     */
    @GetMapping("/getImageListByType")
    public Result<List<Image>> getImageListByType(@RequestParam String type) {
        System.out.println("获取到的轮播图获取类型为："+type);
        List<Image> list = imageService.findImageListByType(type);
        if (list != null) {
            return Result.success(list);
        }
        return Result.error("获取图片列表失败");
    }

    /**
     * 上传图片  @RequestPart注解允许上传最大为10MB的文件
     * @param file 图片
     * @return 相对路径地址
     * @throws IOException IO异常
     */
    @PostMapping("/uploadImage")
    public Result<String> uploadImage(@RequestPart("file") MultipartFile file) throws IOException {
        //把文件的内容存储到本地磁盘上
        String originalFilename = file.getOriginalFilename();
        //保证文件的名字是唯一的，从而防止文件覆盖 截取原始图片名的最后一个'.'之后的字符串，即文件后缀名
        String filename = UUID.randomUUID().toString() + originalFilename.substring(originalFilename.lastIndexOf("."));
        //存储到本地文件夹里
        file.transferTo(new File("D:\\ideaProject\\vue-bigwork\\src\\assets\\" + filename));
        String url = "../src/assets/"+filename;

        if (imageService.addImage(filename,url)) {
            return Result.success("上传图片成功，请查看路径",url);
        }else {
            return Result.error("上传图片失败");
        }
    }

    /**
     * 管理员获取所有图片
     * @return 图片列表
     */
    @GetMapping("/getAllImageList")
    public Result<List<Image>> getAllImageList() {
        List<Image> list = imageService.findAllImage();
        if (list != null) {
            return Result.success(list);
        }
        return Result.error("获取图片列表失败");
    }

    /**
     * 禁用图片
     * @param imageName 图片名称
     * @return 禁用结果
     */
    @GetMapping("/forbiddenImage")
    public Result forbiddenImage(@RequestParam String imageName) {
        boolean forbidden = imageService.forbiddenImage(imageName);
        if (forbidden) {
            return Result.success("禁用图片成功！");
        }
        return Result.error("禁用图片失败！");
    }

    /**
     * 解禁图片
     * @param imageName 图片名称
     * @return 解禁结果
     */
    @GetMapping("/unForbiddenImage")
    public Result unForbiddenImage(@RequestParam String imageName) {
        boolean forbidden = imageService.unForbiddenImage(imageName);
        if (forbidden) {
            return Result.success("解禁图片成功！");
        }
        return Result.error("解禁图片失败！");
    }

    /**
     * 修改图片
     * @param image 图片信息
     * @return 修改结果
     */
    @PostMapping("/editImage")
    public Result editImage(@RequestBody Image image) {
        if (imageService.editImage(image)) {
            return Result.success("修改图片成功！");
        }
        return Result.error("修改图片失败！");
    }
}
