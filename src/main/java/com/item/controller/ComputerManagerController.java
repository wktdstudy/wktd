package com.item.controller;

import com.item.pojo.Computer;
import com.item.pojo.ComputerDailyProfit;
import com.item.pojo.Computerusage;
import com.item.pojo.Result;
import com.item.service.ComputerService;
import com.item.service.ComputerusageService;
import com.item.utils.NotifyToUserWebSocket;
import lombok.AllArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author likaiping
 * @createTime 2024/5/30 16:08
 */
@RestController
@RequestMapping("/computerManager")
@AllArgsConstructor
public class ComputerManagerController {
    private ComputerService computerService;
    private ComputerusageService computerusageService;
    private NotifyToUserWebSocket notifyToUserWebSocket;

    /**
     * 查询所有电脑列表
     * @return 电脑列表
     */
    @GetMapping("/getComputerList")
    public Result<List<Computer>> findComputerList(){
        List<Computer> list = computerService.list();
        return Result.success(list);
    }

    /**
     * 模糊查询电脑列表 筛选
     * @param price 电脑价格
     * @param detail 电脑型号
     * @return computer的list集合
     */
    @GetMapping("/searchComputer")
    public Result findListSearchComputer(@RequestParam String price, @RequestParam String detail) {
        BigDecimal priceValue = null;
        if (!price.equals("")) {
            priceValue = new BigDecimal(price);
        }
        System.out.println("前端拿到的数据：price："+price+" detail："+detail);
        List<Computer> listSearchComputer = computerService.findListSearchComputer(priceValue,detail);
        return Result.success(listSearchComputer);
    }

    /**
     * 添加电脑
     * @param computer 电脑信息
     * @return 响应信息
     */
    @PutMapping("/addComputer")
    public Result addComputer(@RequestBody Computer computer) {
        System.out.println("添加的电脑信息："+computer);
        Boolean aBoolean = computerService.addComputer(computer);
        if (aBoolean) {
            return Result.success("添加电脑成功");
        }
        return Result.error("添加电脑失败");
    }

    /**
     * 删除电脑
     * @param computer 电脑信息
     * @return
     */
    @DeleteMapping("/removeComputer")
    public Result removeComputer(@RequestBody Computer computer){
        System.out.println(computer);
        System.out.println("获取到的电脑id为："+computer.getId());
       if (computerService.removeComputer(computer.getId())){
           return Result.success("删除电脑成功");
       }
       return Result.error("删除电脑失败");
    }

    /**
     * 修改电脑信息
     * @param computer 电脑信息
     * @return 响应结果
     */
    @PostMapping("/editComputer")
    public Result editComputer(@RequestBody Computer computer) {
        if (computerService.editComputer(computer)) {
            return Result.success("修改电脑信息成功");
        }
        return Result.error("修改电脑信息失败");
    }

    @GetMapping("/getComputerUsageCurrent")
    public Result<List<Map<String, String>>> getComputerUsageListByIsDelete(){
        List<Map<String, String>> computerusageList = computerusageService.findComputerUsageListByIsDelete(0);
        return Result.success(computerusageList);
    }

    /**
     * 强制关闭用户电脑
     * @param useInfo 封装了用户使用电脑等信息
     * @return 响应结果
     */
    @PostMapping("/closeUserComputer")
    public Result closeUserComputer(@RequestBody String useInfo) throws JSONException {
        System.out.println("前端传来的String数据："+useInfo);
        JSONObject jsonObject = new JSONObject(useInfo);
        String detail = (String) jsonObject.get("detail");
        String username = (String) jsonObject.get("username");
        String starttime = (String) jsonObject.get("starttime");
        System.out.println("输出json结果："+detail+" "+username+" "+starttime);
        BigDecimal bigDecimal = computerService.useEndAndEditComputerUsage(username, detail);
        return Result.success(bigDecimal);
    }

    /**
     * 发送消息
     * @param message 消息
     * @return 发送结果
     */
    @PostMapping("/notify")
    public Result<String> notifyUsers(@RequestParam String message) {
        try {
//            notificationHandler.sendNotification(message);
            notifyToUserWebSocket.sendAllMessage(message);
            return Result.success("发送消息成功！发送的消息为：" + message);
        } catch (Exception e) {
            e.printStackTrace();
//            throw new RuntimeException(e);
            return Result.error("发送消息失败！");
        }
    }

    /**
     * 管理员查询用户使用电脑历史记录
     * @param username 用户名
     * @param firstTime 查询开始时间
     * @param secondTime 查询结束时间
     * @return 历史记录
     */
    @GetMapping("/managerFindUsageHistory")
    public Result<List<Computerusage>> findComputerUsageAllList(@RequestParam String username, @RequestParam String firstTime, @RequestParam String secondTime) {
        List<Computerusage> list = computerusageService.findComputerUsageAllList(username, firstTime, secondTime);
        return Result.success(list);
    }

    /**
     * 删除历史记录
     * @param id 记录id
     * @return 删除结果
     */
    @DeleteMapping("/removeComputerUsage")
    public Result removeComputerUsage(@RequestParam Integer id) {
        if (computerusageService.removeById(id)) {
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 获取电脑每日收益数据
     * @return 电脑每日收益数据
     */
    @GetMapping("/getComputerDailyProfit")
    public Result<List<ComputerDailyProfit>> getComputerDailyProfit() {
        List<ComputerDailyProfit> list = computerusageService.getComputerDailyProfit();
        if (list != null) {
            return Result.success(list);
        }
        return Result.error("获取电脑每日收益数据失败");
    }

    /**
     * 获取今日收益
     * @return 今日收益
     */
    @GetMapping("/getTodayProfit")
    public Result<ComputerDailyProfit> getTodayProfit() {
        ComputerDailyProfit todayProfit = computerusageService.getTodayProfit();
        return Result.success(todayProfit);
    }

    /**
     * 获取昨日收益
     * @return 昨日收益
     */
    @GetMapping("/getYesterdayProfit")
    public Result<ComputerDailyProfit> getYesterdayProfit() {
        ComputerDailyProfit yesterdayProfit = computerusageService.getYesterdayProfit();
        return Result.success(yesterdayProfit);
    }
 }
