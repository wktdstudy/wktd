package com.item.controller;

import com.item.pojo.RechargeHistory;
import com.item.pojo.Result;
import com.item.service.RechargeRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;


/**
 * @author XXDLD
 */
@RestController
@RequestMapping("/rechargeManager")
public class RechargeManagerController {
    @Autowired
    private RechargeRecordService rechargeRecordService;
    @GetMapping("/getRechargeHistory")
    public Result<List<RechargeHistory>> getRechargeHistory() {
        return Result.success(rechargeRecordService.getRechargeHistory());
    }

}
