package com.item.controller;

import com.item.pojo.*;
import com.item.service.PointsEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author XXDLD
 */
@RestController
@RequestMapping("/pointsManager")
public class PointsManagerController {
    @Autowired
   private PointsEventService pointsEventService;

    /**
     * 修改抽奖项目
     * @PutMapping("/changeLotteryItem")
     * @param lotteryItems 待修改的抽奖项目信息
     * @return 修改结果，成功或失败
     */
    @PutMapping("/changeLotteryItem")
    public Result changeLotteryItem(@RequestBody LotteryItems lotteryItems) {
        if( pointsEventService.changeLotteryItem(lotteryItems)){
            return Result.success("修改成功");
        }
        return Result.error("修改失败");
    }

    /**
     * 添加抽奖项目
     * @PostMapping("/addLotteryItem")
     * @param lotteryItems 待添加的抽奖项目信息
     * @return 添加结果，成功或失败
     */
    @PostMapping("/addLotteryItem")
    public Result addLotteryItem(@RequestBody LotteryItems lotteryItems) {
        if( pointsEventService.addLotteryItem(lotteryItems)){
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 删除抽奖项目
     * @PostMapping("/deleteLotteryItem")
     * @param lottertyItemId 待删除的抽奖项目ID
     * @return 删除结果，成功或失败
     */
    @PostMapping("/deleteLotteryItem")
    public Result deleteLotteryItem(@RequestParam Integer lottertyItemId) {
        if( pointsEventService.deleteLotteryItem(lottertyItemId)){
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    @PostMapping("/changeExchangeItems")
    public Result changeExchangeItems(@RequestBody ExchangeItems exchangeItems) {
        if( pointsEventService.changeExchangeItems(exchangeItems)){
            return Result.success("修改成功");
        }
        return Result.error("修改失败");
    }

    @PostMapping("/changeCoupons")
    public Result changeCoupons(@RequestParam Integer id,@RequestParam Integer price ) {
        if( pointsEventService.changeCoupons(id,price)){
            return Result.success("修改成功");
        }
        return Result.error("修改失败");
    }


    @PostMapping("/addExchangeItems")
    public Result addExchangeItems(@RequestBody ExchangeItems exchangeItems) {
        System.out.println(exchangeItems);
        if( pointsEventService.addExchangeItems(exchangeItems)){
            return Result.success("添加成功");
        }
        return Result.error("添加失败");
    }

    @PostMapping("/deleteExchangeItems")
    public Result deleteExchangeItems(@RequestParam Integer exchangeItemsId) {
        if( pointsEventService.deleteExchangeItems(exchangeItemsId)){
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    @PostMapping("/deleteCoupons")
    public Result deleteCoupons(@RequestParam Integer id) {
        System.out.println(id);
        if( pointsEventService.deleteCoupons(id)){
            return Result.success("删除成功");
        }
        return Result.error("删除失败");
    }

    @GetMapping("/getExchangeItemsRecords")
    public Result getExchangeRecords() {

        List<Map<String,String>> exchangeItemsRecords = pointsEventService.getExchangeItemsRecords();

        List<Map<String,String>> couponsRecords = pointsEventService.getCouponsRecords();

        List<Object> exchangeRecordsList = new ArrayList<>();
        exchangeRecordsList.addAll(exchangeItemsRecords);
        exchangeRecordsList.addAll(couponsRecords);

        return Result.success(exchangeRecordsList);
    }

    @GetMapping("/getAllPointsEvent")
    public Result<List<PointsEvent>> getAllPointsEvent() {
        return Result.success(pointsEventService.getAllPointsEvent());
    }
}
