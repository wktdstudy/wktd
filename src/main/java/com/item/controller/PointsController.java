package com.item.controller;

import com.item.pojo.*;
import com.item.service.PointsEventService;
import com.item.service.userservice;
import com.item.utils.GetUsernameUtil;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author XXDLD
 */
@RequestMapping("/points")
@RestController
@AllArgsConstructor
public class PointsController {
    private userservice userservice;
    private PointsEventService pointsEventService;

    /**
     * 根据用户token获取用户的积分信息。
     *
     * @param token 用户的认证令牌，用于识别用户身份。
     * @return 返回包含用户积分信息的结果对象。
     */
    @GetMapping("/getPoints")
    public Result<List<User>> getPointsByUsername(@RequestHeader("Authorization") String token) {
        String username = GetUsernameUtil.getUsername(token);
        List<User> list = userservice.getPointsByUsername(username);
        return Result.success(username, list);
    }

    /**
     * 用户每日签到，增加积分。
     *
     * @param username 用户名，用于记录签到信息。
     * @return 返回字符串表示签到结果，"true"表示签到成功，"false1"表示今日已签到。
     */
    @PutMapping("/dailyCheckIn")
    public String dailyCheckIn(String username) {
        // 获取当前日期
        LocalDate today = LocalDate.now();

        //获取数据库签到时间
        LocalDate lastCheckinDate = userservice.getCheckinDateByUsername(username);

        // 如果用户今天已经签到，返回false
        if (today.equals(lastCheckinDate)) {
            return "false1";
        }

        /// 更新用户的最后签到日期
        userservice.updateCheckinDateByUsername(username, today);

        // 签到成功，更新积分
        userservice.dailyCheckIn(username);

        //将签到记录插入数据库
        pointsEventService.insertPointsEvent(username);

        return "true";
    }

    /**
     * 根据用户token获取用户的积分事件列表。
     *
     * @param token 用户的认证令牌，用于识别用户身份。
     * @return 返回用户的积分事件列表。
     */
    @GetMapping("/getPointsEvent")
    public List<PointsEvent> getPointsEvent(@RequestHeader("Authorization") String token) {
        String username = GetUsernameUtil.getUsername(token);
        return pointsEventService.getPointsEvent(username);
    }

    /**
     * 通过GET请求获取抽奖物品和优惠券列表
     * 此方法用于处理前端发送的GET请求，以获取可用于抽奖的物品列表和特定条件下的优惠券列表。
     * 特别地，优惠券列表仅包括二级会员和三级会员可使用的优惠券。
     *
     * @return Result<List> 包含抽奖物品和符合条件的优惠券的列表。
     */
    @GetMapping("/getLotteryItems")
    public Result<List> getLotteryItems() {
        List<LotteryItems> lotteryItems = pointsEventService.getLotteryItems();
        return Result.success(lotteryItems);
    }

    /**
     * 处理抽奖请求。
     *
     * 通过GET请求触发抽奖功能，需要用户授权令牌（token）和积分值（pointsValue）作为输入。
     * 使用授权令牌来识别用户，积分值用于确定抽奖的次数或者权重。
     *
     * @param token 用户的授权令牌，用于验证用户身份。
     * @param pointsValue 用户投入的积分值，用于抽奖或者作为抽奖的权重。
     * @return 返回抽奖结果的字符串描述。
     */
    @GetMapping("/lotteryDraw")
    public String lotteryDraw(@RequestHeader("Authorization") String token, Integer pointsValue) {
        String username = GetUsernameUtil.getUsername(token);
        return pointsEventService.lotteryDraw(username,pointsValue);
    }

    /**
     * 通过GET请求获取抽奖记录列表。
     *
     * @return Result<List<Map<String, String>>> - 包含抽奖记录列表的结果对象。
     *         结果对象中，list的每个元素是一个Map，映射了抽奖记录的各个属性和对应的值。
     * @GetMapping("/getLotteryRecords")
     */
    @GetMapping("/getLotteryRecords")
    public Result<List<LotteryRecords>> getLotteryRecords() {
        List<LotteryRecords> lotteryRecords = pointsEventService.getLotteryRecords();
        return Result.success(lotteryRecords);
    }

    /**
     * 将积分转换为余额。
     *
     * @param token        用户的认证令牌，用于识别用户身份。
     * @param pointsNumber 转换的积分数量。
     * @param balance      转换后的余额。
     * @return 返回布尔值，表示积分转换是否成功。
     */
    @GetMapping("/pointsToBalance")
    public String pointsToBalance(@RequestHeader("Authorization") String token, Integer pointsNumber, BigDecimal balance) {
        String username = GetUsernameUtil.getUsername(token);

        return pointsEventService.pointsToBalance(username, pointsNumber, balance);
    }

    /**
     * 获取可兑换物品列表。
     *
     * @return 返回包含可兑换物品列表的结果对象。
     */
    @GetMapping("/getExchangeItems")
    public Result<List> getExchangeItems() {

        List<ExchangeItems> exchangeItems = pointsEventService.getExchangeItems();

        List<Coupons> coupons = pointsEventService.getCoupons();

        List<Object> exchangeList = new ArrayList<>();
        exchangeList.addAll(exchangeItems);

        for (Coupons coupons1 : coupons) {
            if ("一级会员".equals(coupons1.getMember())) {
                exchangeList.add(coupons1);
            }
        }
        return Result.success(exchangeList);
    }

    /**
     * 处理兑换物品类型0的请求。
     * 该方法对应于GET请求的"/exchangeItemsType0"路径，用于处理用户使用优惠券或积分兑换特定类型物品的请求。
     * 通过请求头中的"Authorization"字段获取用户身份信息，然后调用相应的服务方法完成兑换操作。
     *
     * @param token        用户身份验证的令牌，通过请求头传递。
     * @param couponsId    用于兑换的优惠券ID，可能为null，表示不使用优惠券。
     * @param pointsNumber 用于兑换的积分数量，必须为非负数。
     * @return 返回兑换操作的结果字符串，通常包含兑换是否成功的信息。
     */
    @GetMapping("/exchangeItemsType0")
    public String exchangeItemsType0(@RequestHeader("Authorization") String token, Integer couponsId, Integer pointsNumber) {
        String username = GetUsernameUtil.getUsername(token);
        return pointsEventService.exchangeItemsType0(username, couponsId, pointsNumber);
    }

    /**
     * 根据用户积分数量兑换类型1的物品。
     * 此方法处理来自客户端的兑换请求，通过积分数量和用户认证信息来执行兑换操作。
     *
     * @param token        用户的认证令牌，用于验证请求的发送者身份。
     * @param pointsNumber 用户用于兑换的积分数量。
     * @return 返回一个字符串，指示兑换操作的结果，例如成功或失败的消息。
     */
    @GetMapping("/exchangeItemsType1")
    public String exchangeItemsType1(@RequestHeader("Authorization") String token, Integer pointsNumber) {
        String username = GetUsernameUtil.getUsername(token);
        return pointsEventService.exchangeItemsType1(username, pointsNumber);
    }

    /**
     * 通过积分兑换优惠券。
     *
     * @param token        用户的授权令牌，用于验证用户身份。
     * @param couponsId    优惠券的ID，指定用户想要兑换的优惠券。
     * @param pointsNumber 用户用于兑换的积分数量。
     * @return 返回兑换操作的结果，通常是一个描述兑换结果的字符串。
     */
    @GetMapping("/exchangeItemsCoupons")
    public String exchangeItemsCoupons(@RequestHeader("Authorization") String token, Integer couponsId, Integer pointsNumber) {
        String username = GetUsernameUtil.getUsername(token);
        return pointsEventService.exchangeItemsCoupons(username, couponsId, pointsNumber);
    }
}

