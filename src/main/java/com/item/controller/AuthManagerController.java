package com.item.controller;

import com.item.mapper.AuthMapper;
import com.item.pojo.Auth;
import com.item.pojo.Result;
import com.item.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 权限管理
 * @author likaiping
 * @createTime 2024/6/5 15:46
 */
@RestController
@AllArgsConstructor
@RequestMapping("/authManager")
public class AuthManagerController {
    private AuthService authService;

    /**
     * 获取权限列表
     * @return 权限列表
     */
    @GetMapping("/getAuthList")
    public Result<List<Auth>> getAuthList() {
        List<Auth> list = authService.list();
        return Result.success(list);
    }

    /**
     * 获取父级权限列表
     * @return 父级权限列表
     */
    @GetMapping("/getParentAuthList")
    public Result<List<Auth>> getParentAuthList() {
        List<Auth> parentAuthList = authService.getParentAuthList();
        return Result.success(parentAuthList);
    }

    /**
     * 获取父级权限对应的子级权限列表
     * @param pidName 父级权限名称
     * @return 子级权限列表
     */
    @GetMapping("/getChildAuthList")
    public Result<List<Auth>> getChildAuthList(@RequestParam String pidName) {
        List<Auth> list = authService.getChildAuthsListByPidName(pidName);
        if (!list.isEmpty()) {
            return Result.success(list);
        }
        return Result.error("该父级权限没有子权限");
    }

    /**
     * 更新权限信息
     * @param authId 权限id
     * @param name 权限名
     * @param uri 权限路径
     * @return 是否更新成功
     */
    @PostMapping("/editAuthInfo")
    public Result editAuthInfo(@RequestParam Long authId,@RequestParam String name,@RequestParam String uri) {
        System.out.println("接收到的authId, name, uri为：");
        System.out.println(authId+name+uri);
        boolean result = authService.updateAuthInfo(authId, name, uri);
        if (result) {
            return Result.success("修改权限信息成功");
        }
        return Result.error("修改权限信息失败");
    }

    /**
     * 禁用权限
     * @param authId 权限id
     * @return 禁用结果
     */
    @GetMapping("/forbiddenAuth")
    public Result forbiddenAuth(@RequestParam Long authId) {
        if (authService.forbiddenAuth(authId)) {
            return Result.success("禁用权限成功");
        }
        return Result.error("禁用权限失败");
    }

    /**
     * 解禁权限
     * @param authId 权限id
     * @return 解禁结果
     */
    @GetMapping("/unForbiddenAuth")
    public Result unForbiddenAuth(@RequestParam Long authId) {
        if (authService.unForbiddenAuth(authId)) {
            return Result.success("解禁权限成功");
        }
        return Result.error("解禁权限失败");
    }

    /**
     * 添加父级权限
     * @param name 权限名
     * @param uri 权限uri
     * @return 插入结果
     */
    @PutMapping("/addParentAuth")
    public Result addParentAuth(@RequestParam String name,@RequestParam String uri) {
        if (authService.addParentAuth(name, uri)) {
            return Result.success("添加父级权限成功");
        }
        return Result.error("添加父级权限失败");
    }

    /**
     * 添加子级权限
     * @param pidName 父级权限名
     * @param name 子权限名称
     * @param uri 子权限uri
     * @return 插入结果
     */
    @PutMapping("/addChildAuthInParentAuth")
    public Result addChildAuthInParentAuth(@RequestParam String pidName, @RequestParam String name,@RequestParam String uri) {
        if (authService.addChildAuthInParentAuth(pidName, name, uri)) {
            return Result.success("添加子级权限成功");
        }
        return Result.error("添加子级权限失败");
    }

    /**
     * 删除父级权限及其子权限
     * @param pidName 父级权限名称
     * @return 删除结果
     */
    @DeleteMapping("/removeParentAuth")
    public Result removeParentAuth(@RequestParam String pidName) {
        if (authService.removeParentAuth(pidName)) {
            return Result.success("删除父级权限成功");
        }
        return Result.error("删除父级权限失败");
    }

    /**
     * 删除子级权限
     * @param authName 子级权限名称
     * @return 删除结果
     */
    @DeleteMapping("/removeAnChildAuth")
    public Result removeAnChildAuth(@RequestParam String authName) {
        if (authService.removeChildAuth(authName)) {
            return Result.success("删除子级权限成功");
        }
        return Result.error("删除子级权限失败");
    }
}
