package com.item.controller;

import com.item.pojo.RechargeRecord;
import com.item.pojo.Result;
import com.item.service.RechargeRecordService;
import com.item.service.userservice;
import com.item.utils.GetUsernameUtil;
import com.item.utils.RedisUtil;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.math.BigDecimal;
import java.util.List;

/**
 * RechargeController 类：处理与用户充值和余额查询相关的请求
 *
 * @author XXDLD
 */
@RequestMapping("/recharge")
@RestController
@AllArgsConstructor
public class RechargeController {
    private userservice userservice;
    private RechargeRecordService rechargeRecordService;

    /**
     * 获取指定用户名的余额
     *
     * @return Result对象，包含用户名和余额
     */
    @GetMapping("/balance")
    public Result getBalanceByUsername(@RequestHeader("Authorization")String token) {
        String username = GetUsernameUtil.getUsername(token);
        // 通过userservice查询余额
        BigDecimal balance = userservice.getBalanceByUsername(username);
        // 对余额进行四舍五入，保留两位小数
        balance.setScale(2, BigDecimal.ROUND_HALF_UP);
        // 返回Result对象，包含用户名和余额
        return Result.success(username, balance);
    }

    /**
     * 获取指定用户名的充值记录列表
     *
     * @return Result对象，包含用户名和充值记录列表
     */
    @GetMapping("/getRechargeRecord")
    public Result<List<RechargeRecord>> getRechargeRecordByUsersId(@RequestHeader("Authorization")String token) {
        String username = GetUsernameUtil.getUsername(token);
        // 通过rechargeRecordService查询充值记录
        List<RechargeRecord> rechargeRecord = rechargeRecordService.getRechargeRecordByUsersId(username);
        // 返回Result对象，包含用户名和充值记录列表
        return Result.success(username, rechargeRecord);
    }
}
