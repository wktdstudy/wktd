package com.item.controller;

import com.item.pojo.Computer;
import com.item.pojo.Computerusage;
import com.item.pojo.Result;
import com.item.service.ComputerService;
import com.item.utils.GetUsernameUtil;
import com.item.utils.JwtUtil;
import com.item.utils.RedisUtil;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 上机功能  使用computer和computerusage表
 * @author likaiping
 * @createTime 2024/4/21 11:49
 */
@RestController
@RequestMapping("/computer")
@AllArgsConstructor
@EnableScheduling
public class BoardController {
    private ComputerService computerService;
    private RedisUtil redisUtil;

    /**
     * 查询所有电脑列表
     * @return Computer的list集合
     */
//    public List<Computer> listComputer() {
    @GetMapping("/getListComputer")
    public Result findListComputer() {
        List<Computer> list = computerService.list();
        return Result.success(list);
    }
    /**
     * 模糊查询电脑列表 筛选
     * @param price 电脑价格
     * @param detail 电脑型号
     * @return computer的list集合
     */
    @GetMapping("/getSearchComputer")
    public Result findListSearchComputer(@RequestParam String price, @RequestParam String detail) {
        BigDecimal priceValue = null;
        if (!price.equals("")) {
            priceValue = new BigDecimal(price);
        }
        System.out.println("前端拿到的数据：price："+price+" detail："+detail);
        List<Computer> listSearchComputer = computerService.findListSearchComputer(priceValue,detail);
        return Result.success(listSearchComputer);
    }

    /**
     * 上机接口 用户点击上机按钮后发送请求
     * @param detail 电脑机型
     * @return /
     */
    @PutMapping("/insertUsage")
    public Result<Map<String, String>> insertComputerUsage(@RequestParam String detail, @RequestHeader("Authorization") String token) {
        System.out.println("前端传过来的detail："+detail);
        String username = GetUsernameUtil.getUsername(token);
        System.out.println("请求头的token中获取到的username为："+username);
//        String username = (String) redisUtil.get("username");
        System.out.println("username："+username);
        Map<String, String> map = computerService.insertComputerUsage(username, detail);
        return Result.success(map);
    }

    /**
     * 用户结束使用 下机 传入机型
     * @param detail 电脑机型
     * @return
     */
    @PostMapping("/endUseComputer")
    public Result useEndAndEditComputerUsage(String detail, @RequestHeader("Authorization") String token) {
        String username = GetUsernameUtil.getUsername(token);
//        String username = (String) redisUtil.get("username");
        BigDecimal cost = computerService.useEndAndEditComputerUsage(username, detail);
        return Result.success(cost);
    }

    /**
     * 获取用户全部上机历史
     * @return computerUsage列表
     */
    @GetMapping("/findUsageHistory")
    public Result<List<Computerusage>> findComputerUsageHistoryList(@RequestHeader("Authorization") String token) {
        String username = GetUsernameUtil.getUsername(token);
//        String username = (String) redisUtil.get("username");
        List<Computerusage> list = computerService.findComputerUsageListByUserId(username);
        return Result.success(list);
    }

    /**
     * 查询用户指定时间段内的上机历史
     * @param firstTime 第一个时间
     * @param secondTime 第二个时间
     * @return 历史记录
     */
    @GetMapping("/findHistoryByTimeToTime")
    public Result<List<Computerusage>> findHistoryByTimeToTime(@RequestParam String firstTime, @RequestParam String secondTime, @RequestHeader("Authorization") String token) {
        String username = GetUsernameUtil.getUsername(token);
        List<Computerusage> list = computerService.findHistoryByTimeToTime(firstTime,secondTime,username);
        return Result.success(list);
    }

    /**
     * 查询可用时长和余额
     * @param detail 电脑信息
     * @return map对象，存储String类型的时长和余额
     */
    @GetMapping("/getUsableInfo")
    public Result<Map<String,String>> findUsableTimeAndBalance(@RequestParam String detail, @RequestHeader("Authorization") String token) {
        String username = GetUsernameUtil.getUsername(token);
//        String username = (String) redisUtil.get("username");
        Map<String, String> map = computerService.findUsableTimeAndBalance(username, detail);
        return Result.success(map);
    }

}
