package com.item.controller;

import com.item.pojo.*;
import com.item.service.ManagerService;
import com.item.service.MessagesService;
import com.item.service.QuestionService;
import com.item.utils.GetUsernameUtil;
import com.item.utils.RedisUtil;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author XXDLD
 */
@RequestMapping("/customerService")
@RestController
@AllArgsConstructor
public class CustomerController {

    @Autowired
    private QuestionService questionService;
    private ManagerService managerService;
    private MessagesService messagesService;

    /**
     * 通过点击次数获取问题列表。
     *
     * <p>该接口不需要接收任何参数，而是调用questionService中的getQuestionByHits方法，
     * 以获取按点击次数排序的问题列表。这对于展示热门问题非常有用。
     *
     * @return 返回一个Result对象，其中包含一个List<Question>类型的热门问题列表和当前用户名（来自Redis）。
     */
    @GetMapping("/getQuestionByHits")
    public Result<List<Question>> getQuestionByHits(@RequestHeader("Authorization") String token){
        String username = GetUsernameUtil.getUsername(token);
        List<Question> questionLists = questionService.getQuestionByHits();
        return Result.success(username,questionLists);
    }

    /**
     * 根据问题ID获取答案。
     *
     * @param questionId 问题的唯一标识符。
     * @return 返回与问题ID对应的答案字符串。
     */
    @GetMapping("/getAnswerByQuestionId")
    public List<Answer> getAnswerByQuestionId(Integer questionId){
        // 通过问题服务获取指定问题ID的答案
        return questionService.getAnswerByQuestionId(questionId);
    }

    /**
     * 根据问题内容获取问题列表。
     *
     * @param content 问题的内容，用于查询匹配的问题。
     * @return 返回一个包含匹配问题的列表。
     */
    @GetMapping("/getQuestionByContent")
    public List<Question> getQuestionByContent(String content){
        if(content==""){
            return null;
        }else {
            // 通过问题服务根据内容查询问题
            return questionService.getQuestionByContent(content);
        }
    }

    /**
     * 根据管理员姓名获取管理员状态列表。
     *
     * @param token 通过请求头Authorization传递的令牌，用于管理员姓名身份验证。
     * @return 返回一个包含操作成功与否以及用户名和经理状态列表的结果对象。
     */
    @GetMapping("/getManagerStatusByManagerName")
    public Result<List<Manager>> getManagerStatusByManagerName(@RequestHeader("Authorization") String token){
        String username = GetUsernameUtil.getUsername(token);
        List<Manager> managerLists = managerService.getManagerStatusByManagerName();
        return Result.success(username,managerLists);

    }


    /**
     * 通过管理员姓名开启经理状态。
     *
     * @param managerName 需要开启状态的管理员姓名。
     * @return 返回一个布尔值，表示操作是否成功。
     */
    @PutMapping("/updateOnByManagerName")
    public Boolean updateOnByManagerName(String managerName){
        return managerService.updateOnByManagerName(managerName);
    }


    /**
     * 通过管理员姓名关闭在线状态。
     *
     * @param managerName 需要关闭状态的管理员姓名。
     * @return 返回一个布尔值，表示操作是否成功。
     */
    @PutMapping("/updateOffByManagerName")
    public Boolean updateOffByManagerName(String managerName){
        return managerService.updateOffByManagerName(managerName);
    }

    @GetMapping("/getMessagesByUserNames")
    private List<Messages> getMessagesByuserNames(String fromUserName, String toUserName){
        return messagesService.getMessagesByUserNames(fromUserName,toUserName);
    }
}
