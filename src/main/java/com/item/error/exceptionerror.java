package com.item.error;

import com.item.pojo.Result;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class exceptionerror {
@ExceptionHandler(Exception.class)
public Result handleException(Exception e){
    e.printStackTrace();
    return Result.error(StringUtils.hasLength(e.getMessage())? e.getMessage() :"操作有误");

}
}//做全局的错误异常处理
