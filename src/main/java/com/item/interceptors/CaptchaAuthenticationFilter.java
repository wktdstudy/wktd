//package com.item.interceptors;
//
//import com.item.service.item.captchaService;
//import lombok.AllArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.AuthenticationServiceException;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import org.springframework.stereotype.Component;
//
//@Slf4j
//
//public class CaptchaAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
//    private final captchaService captchaService;
//    public CaptchaAuthenticationFilter(captchaService captchaService, AuthenticationManager authenticationManager) {
//        super.setAuthenticationManager(authenticationManager);
//        this.captchaService = captchaService;
//    }
//    @Override
//    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
//        if (!"POST".equals(request.getMethod())) {
//            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
//        }
//
//        String captcha = request.getParameter("captcha");
//        if (captcha == null || !captchaService.comfirmcaptcha(captcha)) {
//            throw new AuthenticationServiceException("验证码错误");
//        }
//
//        return super.attemptAuthentication(request, response);
//    }
//}
