package com.item.interceptors;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.item.service.item.captchaService;
import com.item.utils.JwtUtil;
import com.item.utils.RedisUtil;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Component
@AllArgsConstructor
public class SecurityTokenProcessFilter extends GenericFilterBean {
private captchaService captchaService;
    private StringRedisTemplate customRedisTemplate;
    private ObjectMapper objectMapper;
    private RedisUtil redisUtil;
    @SneakyThrows
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
        jakarta.servlet.http.HttpServletRequest request = (jakarta.servlet.http.HttpServletRequest) servletRequest;
        jakarta.servlet.http.HttpServletResponse response = (jakarta.servlet.http.HttpServletResponse) servletResponse;
        servletResponse.setContentType("application/json;charset=utf-8");
        System.out.println("lanjie");
        // 默认路径直接放行
        if (Objects.equals(request.getRequestURI(), "/user/login")) {
            filterChain.doFilter(servletRequest, servletResponse);
            System.out.println("denglu");
            return;
        }
        if (Objects.equals(request.getRequestURI(), "/user/captcha")) {
            filterChain.doFilter(servletRequest, servletResponse);
            System.out.println("yanzhengma");
            return;
        }
        if (Objects.equals(request.getRequestURI(), "/user/register")) {
            filterChain.doFilter(servletRequest, servletResponse);
            System.out.println("register");
            return;
        }
        if (Objects.equals(request.getRequestURI(), "/user/sendcode")) {
            filterChain.doFilter(servletRequest, servletResponse);
            System.out.println("sendcode");
            return;
        }
        if (Objects.equals(request.getRequestURI(), "/alipay/pay")) {
            filterChain.doFilter(servletRequest, servletResponse);
            System.out.println("alipay/pay");
            return;
        }
        if (Objects.equals(request.getRequestURI(), "/alipay/notify")) {
            filterChain.doFilter(servletRequest, servletResponse);
            System.out.println("alipay/notify");
            return;
        } if (Objects.equals(request.getRequestURI(), "/food/updateuserimageurl")) {
            filterChain.doFilter(servletRequest, servletResponse);
            System.out.println("/food/updateuserimageurl");
            return;
        }if (Objects.equals(request.getRequestURI(), "/notifyToUser")) {
            filterChain.doFilter(servletRequest, servletResponse);
            System.out.println("/notifyToUser");
            return;
        }
        if (request.getRequestURI().startsWith("/imserver/")) {
            filterChain.doFilter(servletRequest, servletResponse);
            System.out.println("/imserver");
            return;
        }
        if (request.getRequestURI().startsWith("/image/getImageListByType")) {
            filterChain.doFilter(servletRequest, servletResponse);
            System.out.println("/image/getImageListByType");
            return;
        }




        // 已经在上下文中进行了认证放行
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            filterChain. doFilter(servletRequest, servletResponse);
            System.out.println("yi ren zheng");
            return;
        }


        String token = request.getHeader("Authorization");
        if (StringUtils.isEmpty(token)) {
            // 未提供认证令牌
            System.out.println("token is null");
            response.getWriter().write("token is null");
            return;
        }

        String username;
        try {
            Map<String, Object> claims = JwtUtil.parseToken(token);
            username = (String) claims.get("username");
        } catch (Exception e) {
            // 令牌无效或过期
            response.getWriter().write("令牌无效或过期");
            return;
        }
        System.out.println(token);
       Object userJsonobject= redisUtil.hashGet("token",username);

        System.out.println("Authentication object: " + userJsonobject);
        if (userJsonobject == null) {

            response.getWriter().write("认证信息错误");
            return;
        }


        System.out.println("authenticationObject" + userJsonobject + "认证成功");
        SecurityContextHolder.getContext().setAuthentication((Authentication) userJsonobject);
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
