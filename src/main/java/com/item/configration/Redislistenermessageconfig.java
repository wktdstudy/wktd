package com.item.configration;

import com.item.utils.redislistenerutil;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

@Configuration
@AllArgsConstructor
public class Redislistenermessageconfig {
    private redislistenerutil redislistenerutil;
    @Bean
    public RedisMessageListenerContainer redisMessageListenerContainer(
            RedisConnectionFactory redisConnectionFactory,
            MessageListener userMessageListener,
            MessageListener ortheruserMessageListener,
            MessageListener couponUserMessageListener
           ) {

        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(redisConnectionFactory);
        container.addMessageListener(userMessageListener, new PatternTopic("common:topic"));
        container.addMessageListener(ortheruserMessageListener, new PatternTopic("orther:topic"));
        container.addMessageListener(couponUserMessageListener, new PatternTopic("coupon:topic"));
        return container;
    }

}
