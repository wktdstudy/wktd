package com.item.configration;

import com.item.Handler.CustomAuthenticationFailureHandler;
import com.item.Handler.CustomAuthenticationSuccessHandler;
import com.item.Handler.SecurityAuthenticationAccessHandler;
import com.item.Handler.SecurityLogoutSuccessHandler;
import com.item.interceptors.SecurityTokenProcessFilter;
import com.item.service.item.UserDetailsServiceImpl;
import com.item.service.item.captchaService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.AntPathMatcher;


@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class SecurityConfig {
    private final UserDetailsServiceImpl userDetailsService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AntPathMatcher antPathMatcher(){

        return new AntPathMatcher();
    }

    private final SecurityTokenProcessFilter securityTokenProcessFilter;


@Bean
@DependsOn({"antPathMatcher","customAuthenticationSuccessHandler","customAuthenticationFailureHandler"})
public SecurityFilterChain securityFilterChain(HttpSecurity http, AntPathMatcher antPathMatcher, CustomAuthenticationFailureHandler customAuthenticationFailureHandler, CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler, SecurityAuthenticationAccessHandler securityAuthenticationAccessHandler, SecurityLogoutSuccessHandler securityLogoutSuccessHandler) throws Exception {
            http.csrf().disable();

        http.cors(); // 启用跨域支持
    http.addFilterBefore(securityTokenProcessFilter, UsernamePasswordAuthenticationFilter.class);
    http.formLogin().loginPage("/login").loginProcessingUrl("/user/login");
       http.formLogin(customizer -> customizer
               .successHandler(customAuthenticationSuccessHandler)
               .failureHandler(customAuthenticationFailureHandler)

       );
       //自定义授权规则
       http.authorizeHttpRequests((authorize) -> authorize
               .requestMatchers("/user/login","/user/logout","/user/register","/user/sendcode","/user/captcha","/user/checkuser","/food/updateuserimageurl").permitAll()
               .requestMatchers("/static/**").permitAll()
               .requestMatchers("/alipay/**").permitAll()
               .requestMatchers("/imserver/**").permitAll()
               .requestMatchers("/notifyToUser").permitAll()
               .requestMatchers("/image/getImageListByType").permitAll()
               .anyRequest().access((authentication, request) ->
                     securityAuthenticationAccessHandler.check(authentication,request.getRequest())

       ));
    System.out.println(userDetailsService);
    return http
            .build();
}
}
