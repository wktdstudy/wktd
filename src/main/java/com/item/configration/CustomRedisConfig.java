package com.item.configration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.security.core.Authentication;

@Configuration
public class CustomRedisConfig {

    private final RedisConnectionFactory redisConnectionFactory;

    @Autowired
    public CustomRedisConfig(RedisConnectionFactory redisConnectionFactory) {
        this.redisConnectionFactory = redisConnectionFactory;
    }

    @Bean("customRedisTemplate")
    public RedisTemplate<String, Authentication> customRedisTemplate() {
        RedisTemplate<String, Authentication> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);

        // 设置 key 的序列化器
        template.setKeySerializer(new StringRedisSerializer());
        // 设置 value 的序列化器，使用 Jackson2JsonRedisSerializer 实现
        template.setValueSerializer(new Jackson2JsonRedisSerializer<>(Authentication.class));

        return template;
    }
}
