package com.item.Handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;

@Component("logoutSuccessHandler")
@AllArgsConstructor
public class SecurityLogoutSuccessHandler implements LogoutSuccessHandler {


    private ObjectMapper objectMapper;

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setContentType("application/json;charset=UTF-8");
        HashMap<String, Object> result = new HashMap<>();
        result.put("code", "20000");
        result.put("message", "退出成功");
        response.getWriter().write(objectMapper.writeValueAsString(result));
    }
}